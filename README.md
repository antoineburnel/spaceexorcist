# Space Exorcist

## --<[ Summary ]>--

This game is made with **Unity**.

The goal of this project is to develop a top-down spaceship game, i'll try to create those game modes :

| Mode | Description
| :-:  | :-:
| **Survival** | Infinite hordes (harder and harder)
| **Campaign** | Levels with objectives and litte story

I have a unique idea for the gameplay, that's why this came is called "**Space exorcist**" (no spoil ftm)

I also have for objective to make the cleanest project.
With SOLID principles, desing patterns, data file and more.

## --<[ Download ]>--

Here is a link to my game : https://deep-vertex-studio.itch.io/space-exorcist-demo (alpha version)


## --<[ Roadmap ]>--

Since it is one of my biggest project, I can promise nothing.

Here is the roadmap in chronogical order :

- [x] Fun movement controls
- [x] Shooting system
- [x] Fun camera
- [x] Black holes and planets
- [x] Gravity, anti-gravity
- [x] Stars in the background
- [x] Pilotable Mothership (safe zone)
- [x] Anti-bullet field
- [x] First enemy entity
- [x] Landing areas
- [x] Multiple weapons on a spaceship
- [x] Moving enemy
- [x] Timer to measure survival time
- [x] Pause Menu
- [x] First level of survival
- [ ] Main menu
- ...

## --<[ The team ]>--

- Antoine BURNEL (myself)

## --<[ My last progress video ]>--
### **(23 / 04 / 2023)** SpaceExorcist - Preview **21** - **AI optimization and fixes**
[![Thumbnail of my lastpreview video](https://img.youtube.com/vi/HNxxmtR5WFQ/0.jpg)](https://www.youtube.com/watch?v=HNxxmtR5WFQ)