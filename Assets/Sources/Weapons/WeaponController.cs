namespace Ship.Weapon
{
    using UnityEngine;
    using Bullets;
    using System.Collections.Generic;

    public class WeaponController
    {

        private static float TIME_TO_HIDE_WEAPON = 0.2f;
        private static float TIME_TO_SHOW_WEAPON = 0.2f;

        private Transform _transform;
        private List<Transform[]> _canons;
        private bool _isReloading;
        private bool _canFire;
        private Spaceship _spaceship;
        private BasicWeaponData[] _datas;
        private Transform[] _sprites;
        private int _currentWeaponIndex;
        private WeaponControllerCoroutine _weaponControllerCoroutine;
        private IWeaponControllerStrategy _weaponControllerStrategy;
        private Rigidbody2D _rb;
        private bool _changingWeapon;

        public WeaponController(Spaceship ship, Transform transform, BasicWeaponData data1, BasicWeaponData data2, IWeaponControllerStrategy strategy)
        {
            _transform = transform;
            _weaponControllerStrategy = strategy;
            _datas = new BasicWeaponData[2];
            _datas[0] = data1;
            _datas[1] = data2;
            _sprites = new Transform[2];
            _sprites[0] = _transform.GetChild(0).GetChild(4).GetChild(1).GetChild(0);
            if (data2 == null) _sprites[1] = null;
            else _sprites[1] = _transform.GetChild(0).GetChild(4).GetChild(1).GetChild(1);
            _changingWeapon = false;
            _currentWeaponIndex = 0;
            _weaponControllerCoroutine = ship.gameObject.AddComponent<WeaponControllerCoroutine>();
            _canFire = true;
            _spaceship = ship;
            _rb = transform.GetComponent<Rigidbody2D>();
            _weaponControllerCoroutine.Init(this);
            InitCanons();
        }

        public void SetWeapon1()
        {
            if (_changingWeapon || _currentWeaponIndex == 0)
            {
                return;
            }
            _changingWeapon = true;
            _currentWeaponIndex = 0;
            _spaceship.GetUiManager().ChangingWeapon();
            _spaceship.GetUiManager().UnsetWeapon();
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.HideWeapon(TIME_TO_HIDE_WEAPON, _sprites[1], _spaceship.GetUiManager().SetWeapon1));
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.HasChangedWeapon(TIME_TO_HIDE_WEAPON + TIME_TO_SHOW_WEAPON / 2f, _spaceship, _datas[_currentWeaponIndex]));
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.ShowWeapon(TIME_TO_HIDE_WEAPON, TIME_TO_SHOW_WEAPON, _sprites[0], HasChangedWeapon));
        }

        public void SetWeapon2()
        {
            if (_changingWeapon || _currentWeaponIndex == 1)
            {
                return;
            }
            if (_datas[1] == null)
            {
                return;
            }
            _changingWeapon = true;
            _currentWeaponIndex = 1;
            _spaceship.GetUiManager().ChangingWeapon();
            _spaceship.GetUiManager().UnsetWeapon();
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.HideWeapon(TIME_TO_HIDE_WEAPON, _sprites[0], _spaceship.GetUiManager().SetWeapon2));
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.HasChangedWeapon(TIME_TO_HIDE_WEAPON + TIME_TO_SHOW_WEAPON / 2f, _spaceship, _datas[_currentWeaponIndex]));
            _weaponControllerCoroutine.StartCoroutine(_weaponControllerCoroutine.ShowWeapon(TIME_TO_HIDE_WEAPON, TIME_TO_SHOW_WEAPON, _sprites[1], HasChangedWeapon));

        }

        private void InitCanons()
        {
            _canons = new List<Transform[]>(2);
            InitCanon(0);
            InitCanon(1);
        }

        private void InitCanon(int index)
        {
            Transform parentOfCanons = _transform.GetChild(0).GetChild(4).GetChild(1).GetChild(index);
            int nbOfCanons = parentOfCanons.childCount - 1;
            _canons.Add(new Transform[nbOfCanons]);
            for (int i = 0; i < nbOfCanons; i++)
            {
                _canons[index][i] = parentOfCanons.GetChild(i + 1);
            }
        }

        public void SetStrategy(IWeaponControllerStrategy strategy)
        {
            _weaponControllerStrategy = strategy;
        }

        public void Fire(int layer)
        {
            BasicWeaponData currentWeaponData = _datas[_currentWeaponIndex];
            _weaponControllerCoroutine.CanFireAfter(currentWeaponData.GetTimeBetweenBullets());
            currentWeaponData.DecreaseCurrentBullet();
            _spaceship.GetUiManager().OnFire(currentWeaponData);
            _canFire = false;
            foreach (Transform canon in _canons[_currentWeaponIndex])
            {
                GameObject bulletObject = Spaceship.Instantiate(currentWeaponData.GetBulletPrefab(), canon.position, canon.rotation);
                bulletObject.layer = layer;
                Bullet bulletScript = bulletObject.AddComponent<ClassicBullet>();
                bulletScript.Init(bulletObject.transform, currentWeaponData.GetBulletData(), _transform);
                bulletScript.StartMoving(canon, _rb);
            }
        }

        public void StartReload()
        {
            _isReloading = true;
            _canFire = false;
            _weaponControllerCoroutine.ReloadDuring(_datas[_currentWeaponIndex].GetReloadTime());
            _spaceship.GetUiManager().OnStartReload(_datas[_currentWeaponIndex], _datas[_currentWeaponIndex].GetReloadTime());
        }

        public void SetPossibilityToFireAgain()
        {
            _canFire = true;
        }

        private void HasChangedWeapon()
        {
            _changingWeapon = false;
        }

        public void ReloadWeapon()
        {
            _datas[_currentWeaponIndex].ReloadWeapon();
            _isReloading = false;
            _canFire = true;
            _spaceship.GetUiManager().OnReload(_datas[_currentWeaponIndex]);
        }

        public void UpdateStrategy()
        {
            _weaponControllerStrategy.Update();
        }

        public bool IsAutomatic()
        {
            return !_datas[_currentWeaponIndex].IsSemiAutomatic();
        }

        public bool IsAbleToFire()
        {
            return _canFire && !_isReloading && !_datas[_currentWeaponIndex].IsMagEmpty() && !_changingWeapon;
        }

        public bool IsAbleToReload()
        {
            return !_isReloading && !_datas[_currentWeaponIndex].IsMagFull() && !_changingWeapon && _canFire;
        }

        public WeaponData GetData()
        {
            return _datas[_currentWeaponIndex];
        }

        public WeaponData GetData(int index)
        {
            return _datas[index];
        }
    }
}