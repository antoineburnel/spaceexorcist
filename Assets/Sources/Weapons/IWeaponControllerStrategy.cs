namespace Ship.Weapon
{
    public interface IWeaponControllerStrategy
    {
        public void Update();
        public void Init(WeaponController weaponController);
    }
}