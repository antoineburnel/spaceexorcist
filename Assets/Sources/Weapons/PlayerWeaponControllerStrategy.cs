namespace Ship.Weapon
{
    using UnityEngine;
    using Utils;

    public class PlayerWeaponControllerStrategy : IWeaponControllerStrategy
    {
        private WeaponController _weaponController;

        public void Init(WeaponController weaponController) {
            _weaponController = weaponController;
        }

        private void TryToFire() {
            if (!_weaponController.IsAbleToFire()) {
                return;
            }
            _weaponController.Fire( (int)LayerMeaning.PLAYER_BULLET );
        }

        private void TryToReload() {
            if (!_weaponController.IsAbleToReload()) {
                return;
            }
            _weaponController.StartReload();
        }

        public void Update()
        {
            bool isAutomatic = _weaponController.IsAutomatic();
            bool holdFireButton = Input.GetMouseButton(0);
            bool clickFireButton = Input.GetMouseButtonDown(0);
            bool wantToSetWeapon1 = Input.GetKeyDown(KeyCode.Z);
            bool wantToSetWeapon2 = Input.GetKeyDown(KeyCode.X);

            if ((isAutomatic && holdFireButton) || clickFireButton) {
                TryToFire();
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                TryToReload();
            }
            if ( wantToSetWeapon1 ) {
                _weaponController.SetWeapon1();
            }
            if ( wantToSetWeapon2 ) {
                _weaponController.SetWeapon2();
            }
        }
    }
}