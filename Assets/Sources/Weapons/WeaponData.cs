namespace Ship.Weapon
{
    using UnityEngine;
    using Bullets;

    public abstract class WeaponData
    {
        protected float _reloadTime;
        protected int _magSize;
        protected int _currentBulletInMag;
        protected float _timeBetweenBullets;
        protected bool _semiAutomatic;
        protected GameObject _bulletPrefab;
        protected BulletData _bulletData;

        public WeaponData(WeaponBaseData data) {
            CopyStats(data);
            ReloadWeapon();
        }

        private void CopyStats(WeaponBaseData data) {
            _reloadTime = data.reloadTime;
            _magSize = data.magSize;
            _timeBetweenBullets = data.timeBetweenBullets;
            _semiAutomatic = data.semiAutomatic;
            _bulletPrefab = data.bulletPrefab;
            _bulletData = data.bulletData;
        }

        public float GetReloadTime() {
            return _reloadTime;
        }

        public int GetMagSize() {
            return _magSize;
        } 

        public float GetTimeBetweenBullets() {
            return _timeBetweenBullets;
        }

        public bool IsSemiAutomatic() {
            return _semiAutomatic;
        }

        public GameObject GetBulletPrefab() {
            return _bulletPrefab;
        }

        public int GetCurrentBulletInMag() {
            return _currentBulletInMag;
        }

        public bool IsMagEmpty() {
            return _currentBulletInMag == 0;
        }

        public bool IsMagFull() {
            return _currentBulletInMag == _magSize;
        }

        public BulletData GetBulletData() {
            return _bulletData;
        }

        public void ReloadWeapon() {
            _currentBulletInMag = _magSize;
        }

        public void DecreaseCurrentBullet() {
            _currentBulletInMag--;
        }
    }
}