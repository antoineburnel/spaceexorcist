namespace Ship.Weapon
{
    using System.Collections;
    using UnityEngine;
    using Utils;
    using System;

    public class WeaponControllerCoroutine : MonoBehaviour
    {
        private WeaponController _weaponController;
        private Coroutine _canFireCoroutine;
        private Coroutine _isReloadingCoroutine;

        public void Init(WeaponController weaponController) {
            _weaponController = weaponController;
        }

        public bool CanFireAfter(float delay)
        {
            if (_canFireCoroutine != null) return false;
            _canFireCoroutine = StartCoroutine(IE_CanFireAfter(delay));
            return true;
        }

        private IEnumerator IE_CanFireAfter(float delay)
        {
            yield return new WaitForSeconds(delay);
            _weaponController.SetPossibilityToFireAgain();
            _canFireCoroutine = null;
        }

        public bool ReloadDuring(float delay)
        {
            if (_isReloadingCoroutine != null) return false;
            _isReloadingCoroutine = StartCoroutine(IE_ReloadDuring(delay));
            return true;
        }

        private IEnumerator IE_ReloadDuring(float delay)
        {
            yield return new WaitForSeconds(delay);
            _weaponController.ReloadWeapon();
            _isReloadingCoroutine = null;
        }

        public IEnumerator HideWeapon(float duration, Transform weapon, Action method)
        {
            Vector3 originalScale = weapon.localScale;
            Vector3 finalScale = new Vector3( 0f, 0f, 1f );
            float elapsed = 0f;
            float t = 0f;

            while ( elapsed < duration ) {
                elapsed += Time.unscaledDeltaTime;
                t = elapsed / duration;
                weapon.localScale = Vector3.Lerp( originalScale, finalScale, SmoothFunction.EaseInSquare(t) );
                yield return null;
            }

            weapon.localScale = finalScale;
            weapon.gameObject.SetActive( false );
            method();
        }

        public IEnumerator ShowWeapon(float offset, float duration, Transform weapon, Action method)
        {
            yield return new WaitForSeconds(offset);

            weapon.gameObject.SetActive( true );

            Vector3 originalScale = new Vector3( 0f, 0f, 1f );
            Vector3 finalScale = new Vector3( 1f, 1f, 1f );
            float elapsed = 0f;
            float t = 0f;

            while ( elapsed < duration ) {
                elapsed += Time.unscaledDeltaTime;
                t = elapsed / duration;
                weapon.localScale = Vector3.Lerp( originalScale, finalScale, SmoothFunction.EaseOutSquare(t) );
                yield return null;
            }

            weapon.localScale = finalScale;
            method();
        }

        public IEnumerator HasChangedWeapon( float delay, Spaceship spaceship, WeaponData data )
        {
            yield return new WaitForSeconds(delay);
            spaceship.GetUiManager().HasChangedWeapon(data);
        } 
    }
}