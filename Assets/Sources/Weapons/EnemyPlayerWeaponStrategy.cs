namespace Ship.Weapon
{
    using Utils;

    public class EnemyWeaponControllerStrategy : IWeaponControllerStrategy
    {
        private WeaponController _weaponController;

        public void Init(WeaponController weaponController) {
            _weaponController = weaponController;
        }

        private void TryToFire() {
            if (!_weaponController.IsAbleToFire()) {
                TryToReload();
                return;
            }
            _weaponController.Fire( (int)LayerMeaning.ROBOT_BULLET );
        }

        private void TryToReload() {
            if (!_weaponController.IsAbleToReload()) {
                return;
            }
            _weaponController.StartReload();
        }

        public void Update()
        {
            TryToFire();
        }
    }
}