namespace Ship.Weapon
{
    using UnityEngine;
    using Bullets;

    [CreateAssetMenu(fileName = "X.asset", menuName = "ScriptableObjects/Weapon")]
    public class WeaponBaseData : ScriptableObject
    {
        public float reloadTime;
        public int magSize;
        public float timeBetweenBullets;
        public bool semiAutomatic;
        public GameObject bulletPrefab;
        public BulletData bulletData;
    }
}