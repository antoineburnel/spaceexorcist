public interface IZoneSubject<T>
{
    public void Register(IZoneObserver<T> observer);
    public void Unregister(IZoneObserver<T> observer);
    public void NotifyDeath();
}