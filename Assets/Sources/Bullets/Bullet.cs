namespace Bullets
{
    using UnityEngine;
    using System.Collections;
    using Gravity;
    using System.Collections.Generic;

    public abstract class Bullet : MonoBehaviour, IAllAttractionSensitive, IBullet, IBulletAnimation, ICanDie
    {
        protected float GRAVITY_RATIO = 1f;
        protected Transform _transform;
        protected Rigidbody2D _rb;
        protected SpriteRenderer _renderer;
        private BulletData _bulletData;
        protected float _destroyAnimationDuration;
        protected bool _inDestruction;
        protected bool _hasCollideWithSomething;
        protected bool _playingExplosionAnimation;
        protected float _damage;
        private Vector2 _baseVelocity;
        private Transform _shooterPosition;
        private List<IGravitySource> _gravityObservers = new List<IGravitySource>();

        public void Init(Transform bulletTransform, BulletData bulletData, Transform shooterPosition) {
            
            _transform = bulletTransform;
            _rb = _transform.GetComponent<Rigidbody2D>();
            _renderer = _transform.GetComponent<SpriteRenderer>();
            _bulletData = bulletData;
            _renderer.color = _bulletData.color;
            _shooterPosition = shooterPosition;

            _inDestruction = false;
            _destroyAnimationDuration = 0.1f;
            _hasCollideWithSomething = false;
            _playingExplosionAnimation = false;
            _baseVelocity = new Vector2(0, 0);

            _damage = _bulletData.damage;
        }

        public void StartMoving(Transform launcher, Rigidbody2D rb) {
            _baseVelocity = launcher.right * _bulletData.speed;
            _rb.velocity = _baseVelocity + rb.velocity;
            Invoke("PlayDestroyAnimation", _bulletData.durabilitySeconds);
        }

        protected void PlayDestroyAnimation() {
            if (_inDestruction) {
                return;
            }
            _inDestruction = true;
            StartCoroutine(DestroyAnimation());
        }

        private void DisableCollisions() {
            Destroy(_transform.GetComponent<BoxCollider2D>());
        }

        public void DestroyBullet() {
            DestroyImmediate(gameObject);
        }

        public BulletData GetBulletData() {
            return _bulletData;
        }

        public void ResetVelocity() {
            _rb.velocity = _baseVelocity;
        }

        public void DestroyMyself() {
            _hasCollideWithSomething = true;
            PlayDestroyAnimation();
            DisableCollisions();
        }

        public float GetDamage() {
            return _damage;
        }

        public void DestroyOtherBullet(Bullet otherBullet) {
            otherBullet.DestroyMyself();
            _hasCollideWithSomething = true;
            PlayDestroyAnimation();
        }

        public void Die( Transform transform )
        {
            DestroyMyself();
        }

        public void DieFromBlackHole( Transform blackHole ) {
            Die(blackHole);
        }

        public Transform GetEnemyTransform()
        {
            return _shooterPosition;
        }

        public void GravityRegister(IGravitySource source)
        {
            _gravityObservers.Add(source);
        }

        public void GravityUnregister(IGravitySource source)
        {
            _gravityObservers.Remove(source);
        }

        private void UnregisterAll()
        {
            for (int i = _gravityObservers.Count-1; i >= 0; i--) {
                _gravityObservers.RemoveAt(i);
            }
        }

        private void OnDestroy()
        {
            GravitySignalDeath();
            UnregisterAll();
        }

        public void GravitySignalDeath()
        {
            foreach (IGravitySource source in _gravityObservers)
            {
                source.OnDeath(this);
            }
        }

        public abstract void OnCollisionEnter2D ( Collision2D other );

        public abstract void CollisionWithAnotherBullet ( Transform transform );

        public abstract void CollisionWithDestroyableEntity ( Transform transform, ICanBeHit canBeHit );

        public abstract void CollisionWithAWall();

        public abstract void ApplyPlanetAntiGravity(Transform planetTransform, float antiGravityForce, float maxDistance);

        public abstract void ApplyPlanetGravity(Transform planetTransform, float gravityForce, float maxDistance);

        public abstract void ApplyBlackHoleGravity(Transform planetTransform, float gravityForce, float maxDistance);
        
        public abstract IEnumerator DestroyAnimation();

        public abstract void PlayCollideAnimation();
    }
}