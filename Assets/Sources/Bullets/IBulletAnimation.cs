namespace Bullets
{
    using System.Collections;

    public interface IBulletAnimation
    {
        public abstract IEnumerator DestroyAnimation();
        public abstract void PlayCollideAnimation();
    }
}