namespace Bullets
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "X.asset", menuName = "ScriptableObjects/Bullet")]
    public class BulletData : ScriptableObject
    {
        public float speed = 5f;
        public float durabilitySeconds = 5f;
        public int damage = 1;
        public Color32 color = Color.white;
        public EnumGravityResitanceResistance gravityResistance = EnumGravityResitanceResistance.LOW;
    }
}