namespace Bullets
{
    using UnityEngine;
    using System.Collections;
    using Utils;

    public class ClassicBullet : Bullet, IBulletCollision, IBulletAnimation
    {

        public ClassicBullet() {
            GRAVITY_RATIO = 1f/10000f;
        }

        public override IEnumerator DestroyAnimation() {
            float elapsed = 0f;
            Color baseColor = _renderer.color;
            Color finalColor = new Color(baseColor.r, baseColor.g, baseColor.b, 0);
            if (_hasCollideWithSomething) {
                PlayCollideAnimation();
            }
            while (elapsed < _destroyAnimationDuration) {
                _renderer.color = Color.Lerp(baseColor, finalColor, elapsed / _destroyAnimationDuration);
                elapsed += Time.unscaledDeltaTime;
                yield return 0;
            }
            DestroyBullet();
        }

        public override void PlayCollideAnimation() {
            if (_playingExplosionAnimation) {
                return;
            }
            _playingExplosionAnimation = true;
            ParticleSystem particuleSystem = _transform.GetComponent<ParticleSystem>();
            ParticleSystem.ColorOverLifetimeModule colorOverLifetime = particuleSystem.colorOverLifetime;
            Color finalColor = new Color(_renderer.color.r, _renderer.color.g, _renderer.color.b, 0);
            colorOverLifetime.color = new ParticleSystem.MinMaxGradient(_renderer.color, finalColor);
            particuleSystem.Play();
        }

        public override void OnCollisionEnter2D(Collision2D other) {

            Transform otherTransform = other.transform;

            IBulletCollision bulletCollision = otherTransform.GetComponent<IBulletCollision>();
            if ( bulletCollision != null ) {
                CollisionWithAnotherBullet( otherTransform );
                return;
            }

            ICanBeHit canBeHit = otherTransform.GetComponent<ICanBeHit>();
            if ( canBeHit != null) {
                CollisionWithDestroyableEntity( otherTransform, canBeHit );
                return;
            }
            
            CollisionWithAWall();
        }
        
        public override void CollisionWithAnotherBullet ( Transform transform ) {

            if (gameObject.layer != (int)LayerMeaning.PLAYER_BULLET) {
                return;
            }

            Bullet otherBullet = transform.GetComponent<Bullet>();
        
            if (GetDamage() > otherBullet.GetDamage()) {
                ResetVelocity();
                DestroyOtherBullet(otherBullet);
                return;
            }
            else if (GetDamage() < otherBullet.GetDamage()) {
                otherBullet.ResetVelocity();
                DestroyMyself();
                return;
            }

            DestroyOtherBullet(otherBullet);
            DestroyMyself();
        }

        public override void CollisionWithDestroyableEntity ( Transform transform, ICanBeHit canBeHit ) {

            bool iAmAPlayerBullet = (gameObject.layer == (int)(LayerMeaning.PLAYER_BULLET));
            bool myTargetIsAPlayer = (transform.gameObject.layer == (int)(LayerMeaning.PLAYER) || transform.gameObject.layer == (int)(LayerMeaning.CONTROLLED_PLAYER)); 
            
            if (iAmAPlayerBullet && myTargetIsAPlayer)
            {
                CollisionWithAWall();
                return;
            }

            if (!iAmAPlayerBullet && !myTargetIsAPlayer)
            {
                CollisionWithAWall();
                return;
            }

            canBeHit.Hit(_damage, _transform, false);
            _hasCollideWithSomething = true;
            PlayDestroyAnimation();
        }

        public override void CollisionWithAWall () {
            _hasCollideWithSomething = true;
            PlayDestroyAnimation();
        }

        public override void ApplyPlanetGravity(Transform planetTransform, float gravityForce, float maxDistance)
        {
            if ( planetTransform == null ) return;
            float distance = Heuristics.euclideanDistance( planetTransform.position, _transform.position );
            if ( distance > maxDistance ) return;
            gravityForce = (1f - ( distance - maxDistance )) * gravityForce * GRAVITY_RATIO;
            Vector3 direction = (planetTransform.position - _transform.position).normalized;
            _rb.AddForce( gravityForce * direction );
        }

        public override void ApplyPlanetAntiGravity(Transform planetTransform, float antiGravityForce, float maxDistance)
        {
            if ( planetTransform == null ) return;
            float distance = Heuristics.euclideanDistance( planetTransform.position, _transform.position );
            if ( distance > maxDistance ) return;
            antiGravityForce = (1f - ( distance - maxDistance )) * antiGravityForce * GRAVITY_RATIO;
            Vector3 direction = ( _transform.position - planetTransform.position ).normalized;
            _rb.AddForce( antiGravityForce * direction );
        }

        public override void ApplyBlackHoleGravity(Transform planetTransform, float gravityForce, float maxDistance)
        {
            if ( planetTransform == null ) return;
            float distance = Heuristics.euclideanDistance(planetTransform.position, _transform.position);
            gravityForce = (maxDistance - distance) * gravityForce * GRAVITY_RATIO;
            Vector3 direction = planetTransform.position - _transform.position;
            _rb.AddForce(gravityForce * direction);
        }
    }
}