namespace Bullets
{
    using UnityEngine;

    public interface IBullet
    {
        public void Init(Transform bulletTransform, BulletData bulletData, Transform shooterPosition);
        public void StartMoving(Transform launcher, Rigidbody2D rb);
        public BulletData GetBulletData();
        public void DestroyMyself();
        public void DestroyOtherBullet(Bullet otherBullet);
        public void ResetVelocity();
    }
}