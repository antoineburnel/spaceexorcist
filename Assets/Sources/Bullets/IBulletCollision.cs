namespace Bullets
{
    using UnityEngine;
    public interface IBulletCollision
    {
        public abstract void OnCollisionEnter2D ( Collision2D other );

        public abstract void CollisionWithAnotherBullet ( Transform transform );

        public abstract void CollisionWithDestroyableEntity ( Transform transform, ICanBeHit canBeHit );

        public abstract void CollisionWithAWall ();
    }
}