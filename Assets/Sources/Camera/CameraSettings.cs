namespace Ship.Camera
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "X.asset", menuName = "ScriptableObjects/CameraSettings")]
    public class CameraSettings : ScriptableObject
    {
        public float smoothSpeed = 0.125f;
        public Vector3 offset = new Vector3(0, 0, -1);
        public Vector2 distanceCameraProjection = new Vector2(5, 10);
        public float speedToDistanceRatio = 0.1f;
        public float speedToProjectionRatio = 0.05f;
    }
}