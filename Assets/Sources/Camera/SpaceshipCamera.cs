namespace Ship.Camera
{
    using UnityEngine;
    using System.Collections;
    using Utils;

    public class SpaceshipCamera : ISpaceshipEvents {

        private CameraSettings _settings;
        private Camera _camera;
        private Rigidbody2D _rb;
        private Transform _spaceship;
        private Transform _cameraTransform;

        public SpaceshipCamera(Transform spaceship, Rigidbody2D rb, CameraSettings settings) {
            _spaceship = spaceship;
            _rb = rb;
            _camera = Camera.main;
            _cameraTransform = _camera.transform;
            _settings = settings;
        }

        public void Update(Spaceship spaceship, float velocity)
        {      
            _cameraTransform.position = GetNextPosition(spaceship, velocity, _settings);
            _camera.orthographicSize = GetNextFOV(velocity, _settings);
        }

        public Vector3 GetNextPosition(Spaceship spaceship, float velocity, CameraSettings settings)
        {
            float lookAheadDistance = velocity * _settings.speedToDistanceRatio;
            Vector3 lookAheadPosition = _spaceship.position + _spaceship.right * lookAheadDistance;
            
            Vector3 desiredPosition = lookAheadPosition + _settings.offset;
            Vector3 smoothedPosition = Vector3.Lerp(_cameraTransform.position, desiredPosition, _settings.smoothSpeed);

            return smoothedPosition;
        }

        public float GetNextFOV(float velocity, CameraSettings settings)
        {
            float newOrthographicSize = Mathf.Lerp(_settings.distanceCameraProjection.x, _settings.distanceCameraProjection.y, velocity * _settings.speedToProjectionRatio);

            return newOrthographicSize;
        }

        public void ChangingShip(Spaceship ship, float duration)
        {
            ship.StartCoroutine( ChangingShipAnim(ship, duration) );
        }

        private IEnumerator ChangingShipAnim(Spaceship ship, float duration)
        {
            PlayerSpaceship playership = (PlayerSpaceship)ship;

            Vector3 startPos = _cameraTransform.position;
            Vector3 finalPos = playership.GetNextCameraPosition();

            float startFOV = _camera.orthographicSize;
            float finalFOV = playership.GetNextFOV();

            float elapsed = 0f;
            float t = 0f;
            while (elapsed < duration)
            {
                elapsed += Time.unscaledDeltaTime;
                t = elapsed / duration;
                _cameraTransform.position = Vector3.Lerp(startPos, finalPos, SmoothFunction.EaseInOutSquare(t));
                _camera.orthographicSize = Mathf.Lerp(startFOV, finalFOV, SmoothFunction.EaseInOutSquare(t));
                yield return null;
            }
            _cameraTransform.position = finalPos;
            _camera.orthographicSize = finalFOV;
        }
    }
}