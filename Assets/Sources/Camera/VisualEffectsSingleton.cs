using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine;

public class VisualEffectsSingleton : MonoBehaviour, ISpaceshipEvents
{

    public static VisualEffectsSingleton Instance;

    [SerializeField] private Transform _light;
    [SerializeField] private Transform _volume;

    private void Awake()
    {
        Instance = this;
        _globalLight = _light.GetComponent<Light2D>();
        _initialGlobalLightIntensity = _globalLight.intensity;

        _camera = Camera.main;
        _initialBackgroundCamera = _camera.backgroundColor;
        _postProcessingVolume = _volume.GetComponent<Volume>();
        _postProcessingVolume.profile.TryGet<FilmGrain>( out _filmGrain );
        _postProcessingVolume.profile.TryGet<LensDistortion>( out _lensDistortion );
        _postProcessingVolume.profile.TryGet<Vignette>( out _vignette );
        _postProcessingVolume.profile.TryGet<ColorAdjustments>( out _colorAdjustements );
        _postProcessingVolume.profile.TryGet<ChromaticAberration>( out _chromaticAberration );
        _postProcessingVolume.profile.TryGet<DepthOfField>( out _depthOfField );
    }

    private Light2D _globalLight;
    private float _initialGlobalLightIntensity;

    private Camera _camera;
    private Color _initialBackgroundCamera;

    private Volume _postProcessingVolume;
    private FilmGrain _filmGrain;
    private LensDistortion _lensDistortion;
    private Vignette _vignette;
    private ColorAdjustments _colorAdjustements;
    private ChromaticAberration _chromaticAberration;
    private DepthOfField _depthOfField;

    public void SetGlobalLightIntensity ( float percentage )
    {
        _globalLight.intensity = _initialGlobalLightIntensity * percentage;
    }

    public void SetCameraBackgroundIntensity ( float percentage )
    {
        _camera.backgroundColor = Color.Lerp( Color.black, _initialBackgroundCamera, percentage );
    }

    public void SetFilmGrainIntensity ( float percentage )
    {
        _filmGrain.intensity.Override( percentage );
    }

    public void SetLensDistortion ( float percentage )
    {
        _lensDistortion.intensity.Override( percentage );
    }

    public void SetVignette( float percentage ) 
    {
        _vignette.intensity.Override( percentage );
    }

    public void SetColorSaturation ( float percentage )
    {
        _colorAdjustements.saturation.Override( percentage );
    }

    public void SetChromaticAberration ( float percentage )
    {
        _chromaticAberration.intensity.Override ( percentage );
    }

    public void SetDepthOfField ( float percentage )
    {
        _depthOfField.focusDistance.Override ( percentage );
    }
}
