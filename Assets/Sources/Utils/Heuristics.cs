namespace Utils
{   
    using UnityEngine;
    public static class Heuristics {
        public static float euclideanDistance(Vector3 a, Vector3 b) {
            return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
        }
        public static float euclideanDistance(float distanceFromOrigin) {
            return Mathf.Sqrt(distanceFromOrigin * distanceFromOrigin * 2f);
        }
    }
}