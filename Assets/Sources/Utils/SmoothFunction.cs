namespace Utils {

    public interface SmoothFunction {

        public static float fifthOrderSmoothstep(float x) {
            return x * x * x * (6*x*x - 15*x + 10);
        }

        public static float NormalizedReversed2ndDegreePolynom(float t) {
            return 4f*t*(1f-t);
        }

        public static float LinearDescending(float t) {
            return 1f-t;
        }

        public static float LinearAscending(float t) {
            return t;
        }

        public static float EaseInSquare(float t) {
            return t*t;
        }

        public static float EaseInOutSquare(float t) {

            if ( t < 0.5f ) return 2f * EaseInSquare(t);
            else return 1f - ((-2f * t + 2f) * (-2f * t + 2f)) / 2f ;
        }

        public static float EaseOutSquare(float t) {
            return 1 - EaseInSquare(1-t);
        }

        public static float EaseInCubic(float t) {
            return t*t*t;
        }

        public static float EaseOutCubic(float t) {
            return 1 - EaseInCubic(1-t);
        }

        public static float TimeToTriangle(float t) {
            if (t <= 0.5f) return t * 2;
            else return 1f - t;
        }

        public static float TimeToFlatTriangle(float t, float power) {
            if (t <= 1f/power) return t * power;
            if (t >= 1f-(1f/power)) return (1f - t) * power;
            else return 1f;
        }
    }
}