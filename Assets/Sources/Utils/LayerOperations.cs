namespace Utils
{
    using UnityEngine;
    public class LayerOperations
    {
        public static void SetLayerRecursively( GameObject obj, int newLayer )
        {
            obj.layer = newLayer;
        
            for ( int i = 0; i < obj.transform.childCount; i++ )
            {
                SetLayerRecursively( obj.transform.GetChild( i ).gameObject, newLayer );
            }
        }
    }
}