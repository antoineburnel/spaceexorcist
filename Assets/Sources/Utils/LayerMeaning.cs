namespace Utils
{
    public enum LayerMeaning {
        DEFAULT = 0,
        TRANSPARENT_FX = 1,
        IGNORE_RAYCAST = 2,
        WALL = 3,
        WATER = 4,
        UI = 5,
        PLAYER = 6,
        ROBOT = 7,
        ALIEN = 6,
        PLAYER_BULLET = 9,
        ROBOT_BULLET = 10,
        ALIEN_BULLET = 11,
        CONTROLLED_PLAYER = 12,
        PLAYER_MELEE_WEAPON = 13,
        ROBOT_MELEE_WEAPON = 14,
        ALIEN_MELEE_WEAPON = 15,
        GRAVITY_AND_DEATH_SENSOR = 16,
        BULLET_AND_ENEMY_BLOCKER = 17,
    }
}