namespace Utils
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class BasicAnim
    {
        public static IEnumerator MoveObjFromPosToPos ( Transform obj, Vector3 origin, Vector3 arrival, float duration, Action methodToCallAtTheEnd )
        {
            float elapsed = 0;
            while ( elapsed < duration )
            {
                elapsed += Time.unscaledDeltaTime;
                obj.position = Vector3.Lerp( origin, arrival, elapsed / duration );
                yield return null;
            }
            if (methodToCallAtTheEnd != null) methodToCallAtTheEnd();
        }
    }
}