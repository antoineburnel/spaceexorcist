using UnityEngine;
using System.Collections.Generic;

public class StarBackgroundLayer : BackgroundLayer
{
    private int _nbOfStars;
    private Vector2 _dimension;
    private float _speedFactor;
    private float _sizeFactor;
    private List<Sprite> _starSprites;
    private StarInstanciator _spriteInstanciator;
    private Transform _root;
    private Transform _camera;
    private const float DEFAULT_SIZE = 10f;
    private const float DEFAULT_SPEED = 1f;
    private int _order;
    private Vector2 _origin;

    public StarBackgroundLayer(Transform root, Vector2 origin, Transform camera, StarInstanciator starInstanciator, int nbOfStars, Vector2 dimension, float speedDivider, float sizeDivider) : base(root, camera, speedDivider)
    {
        _nbOfStars = nbOfStars * (int)sizeDivider;
        _root = root;
        _spriteInstanciator = starInstanciator;
        _dimension = dimension;
        _sizeFactor = DEFAULT_SIZE / sizeDivider;
        _speedFactor = DEFAULT_SPEED / speedDivider;
        _order = -100 - (int)sizeDivider;
        _origin = origin;
        _starSprites = new List<Sprite>();
        Sprite[] sprites = Resources.LoadAll<Sprite>("Stars");
        foreach (Sprite sprite in sprites)
        {
            _starSprites.Add(sprite);
        }
        GenerateStars();
    }

    private void GenerateStars()
    {
        for (int i = 0; i < _nbOfStars; i++)
        {
            Vector3 position = GetRandomCoords();
            Quaternion rotation = Quaternion.identity;
            Vector3 scale = GetScale();
            Sprite sprite = GetRandomSprite();
            Color color = GetRandomColor();
            _spriteInstanciator.InstantiateStar(sprite, position, rotation, scale, _root, color, _order);
        }
    }

    private Vector3 GetRandomCoords()
    {
        float x = Random.Range(_origin.x, _origin.x + _dimension.x) * _speedFactor;
        float y = Random.Range(_origin.y, _origin.y + _dimension.y) * _speedFactor;
        return new Vector3(x, y, 0);
    }

    private Quaternion GetRandomRotation()
    {
        float z = Random.Range(0, 360);
        return Quaternion.Euler(0, 0, z);
    }

    private Vector3 GetScale()
    {
        return new Vector3(_sizeFactor, _sizeFactor, 1f);
    }

    private Sprite GetRandomSprite()
    {
        int x = Random.Range(0, _starSprites.Count);
        return _starSprites[x];
    }

    public Color GetRandomColor()
    {
        //float x = Random.Range(0f, 1f);
        //return Color.Lerp(new Color32(128, 128, 128, 255), Color.white, x);
        return Color.white;
    }
}
