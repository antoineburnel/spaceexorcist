using UnityEngine;
using System.Collections.Generic;

public class BackgroundLayers : MonoBehaviour
{
    private Vector2 _origin;
    private float _nbOfStarsPerM2;
    private StarInstanciator _starInstanciator;
    private Transform _camera;
    private Vector2 _dimensions;
    private int _nbOfStars;
    private Transform _transform;
    private List<BackgroundLayer> _layers;

    private void Awake()
    {
        _transform = transform;
        _starInstanciator = _transform.GetComponent<StarInstanciator>();
    }

    public void Inject (float nbOfStarsPerM2, Vector2 origin, Vector2 dimensions, Transform camera)
    {
        _nbOfStarsPerM2 = nbOfStarsPerM2;
        _origin = origin;
        _dimensions = dimensions;
        _camera = camera;
        _nbOfStars = (int) (_nbOfStarsPerM2 * _dimensions.x * _dimensions.y);
        _layers = new List<BackgroundLayer>();

        AddPlanetLayer(4, 10f);
        AddPlanetLayer(5, 12.5f);
        AddPlanetLayer(6, 15f);

        AddStarLayer(0, 25f, 3.5f);
        AddStarLayer(1, 30f, 4f);
        AddStarLayer(2, 35f, 4.5f);
        AddStarLayer(3, 40f, 5f);


        AddGalaxyLayer(7, 50f);
    }

    public void UpdateLayers()
    {
        foreach (BackgroundLayer layer in _layers)
        {
            layer.UpdatePositionRelativelyToCamera();
        }
    }

    private void AddStarLayer(int childNumber, float speedDivider, float size)
    {
        if (!_transform.GetChild(childNumber).gameObject.activeSelf) return;
        _layers.Add(new StarBackgroundLayer(_transform.GetChild(childNumber), _origin, _camera, _starInstanciator, _nbOfStars, _dimensions, speedDivider, size));
    }

    private void AddPlanetLayer(int childNumber, float speedDivider)
    {
        if (!_transform.GetChild(childNumber).gameObject.activeSelf) return;
        _layers.Add(new PlanetBackgroundLayer(_transform.GetChild(childNumber), _camera, speedDivider));
    }

    private void AddGalaxyLayer(int childNumber, float speedDivider)
    {
        if (!_transform.GetChild(childNumber).gameObject.activeSelf) return;
        _layers.Add(new GalaxyBackgroundLayer(_transform.GetChild(childNumber), _camera, speedDivider));
    }
}
