using UnityEngine;

public abstract class BackgroundLayer
{

    private Transform _root;
    private Transform _camera;
    private float _speedFactor;
    private const float DEFAULT_SPEED = 1f;

    public BackgroundLayer (Transform root, Transform camera, float speedDivider)
    {
        _root = root;
        _camera = camera;
        _speedFactor = 1f - (DEFAULT_SPEED / speedDivider);
    }

    public void UpdatePositionRelativelyToCamera()
    {
        _root.position = _camera.position * _speedFactor;
    }
}
