using UnityEngine;

public class StarInstanciator : MonoBehaviour
{

    [SerializeField] private GameObject _prefabStar;

    public void InstantiateStar(Sprite sprite, Vector3 position, Quaternion rotation, Vector3 scale, Transform parent, Color color, int order)
    {
        GameObject gameObject = Instantiate(_prefabStar, position, rotation, parent);
        gameObject.transform.localScale = scale;
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
        spriteRenderer.sortingOrder = order;
        spriteRenderer.color = color;
    }
}
