using UnityEngine;
using Ship;

public class ZoneOfDamageAlien : ZoneOfDamage
{
    public override void OnTriggerEnter2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship spaceship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null || spaceship.IsAlien())
        {
            return;
        }

        _objectsInHitZone.Add(spaceship);
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship spaceship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null || spaceship.IsAlien())
        {
            return;
        }

        _objectsInHitZone.Remove(spaceship);
    }
}