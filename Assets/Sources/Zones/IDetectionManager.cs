using UnityEngine;
public interface IDetectionManager
{
    public void EVENT_OnExitDetectionZone(Transform target);
    public void EVENT_OnEnterDetectionZone(Transform target);
    public void EVENT_OnDeath(Transform target);
}