using System.Collections.Generic;
using UnityEngine;
using System;
using Ship;

public abstract class ZoneOfDamage : MonoBehaviour, IZoneObserver<Spaceship>
{
    protected List<Spaceship> _objectsInHitZone;
    protected Transform _transform;

    [SerializeField] protected float DAMAGE_PER_SEC = 1f;
    private float DAMAGE_PER_TICK;

    private void Awake()
    {
        _objectsInHitZone = new List<Spaceship>();
        _transform = transform;
        DAMAGE_PER_TICK = DAMAGE_PER_SEC / 20f;
    }

    public abstract void OnTriggerEnter2D(Collider2D other);

    public abstract void OnTriggerExit2D(Collider2D other);

    private void FixedUpdate()
    {
        for (int i = 0; i < _objectsInHitZone.Count; i++)
        {
            try {
                _objectsInHitZone[i].Hit(DAMAGE_PER_TICK, _transform, false);
            }
            catch (MissingReferenceException) {
                _objectsInHitZone.RemoveAt(i);
                i--;
            }
        }
    }

    public void AddSpaceship(Spaceship spaceship)
    {
        spaceship.Register(this);
        _objectsInHitZone.Add(spaceship);
    }

    public void RemoveSpaceship(Spaceship spaceship)
    {
        spaceship.Unregister(this);
        _objectsInHitZone.Remove(spaceship);
    }

    public Transform GetEnemyTransform()
    {
        return _transform;
    }

    public void OnDeath(Spaceship spaceship)
    {
        _objectsInHitZone.Remove(spaceship);
    }
}