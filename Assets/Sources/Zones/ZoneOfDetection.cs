using System.Collections.Generic;
using UnityEngine;
using Ship;

public abstract class ZoneOfDetection : MonoBehaviour, IZoneObserver<Spaceship>
{
    protected List<IDetectionManager> _observers;

    private void Awake()
    {
        _observers = new List<IDetectionManager>();
    }

    public abstract void OnTriggerEnter2D(Collider2D other);

    public abstract void OnTriggerExit2D(Collider2D other);

    public void AddObserver(IDetectionManager observer)
    {
        _observers.Add(observer);
    }

    public void NotifyEnter(Spaceship spaceship)
    {
        spaceship.Register(this);
        foreach (IDetectionManager observer in _observers)
        {
            observer.EVENT_OnEnterDetectionZone(spaceship.transform);
        }
    }

    public void NotifyExit(Spaceship spaceship)
    {
        spaceship.Unregister(this);
        foreach (IDetectionManager observer in _observers)
        {
            observer.EVENT_OnExitDetectionZone(spaceship.transform);
        }
    }

    public void OnDeath(Spaceship subject)
    {
        foreach (IDetectionManager observer in _observers)
        {
            observer.EVENT_OnDeath(subject.transform);
        }
    }
}