using UnityEngine;
using Gravity;
public class ZoneOfDeathFromBlackHole : MonoBehaviour
{

    private Transform _transform;

    private void Awake ()
    {
        _transform = transform;
    }

    private void OnTriggerEnter2D ( Collider2D other )
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if ( iParentFromCollider == null )
        {
            return;
        }

        ICanDie iCanDie = iParentFromCollider.GetParentTransform().GetComponent<ICanDie>();
        if ( iCanDie == null )
        {
            return;
        }

        IBlackHoleSensitive blackHoleSensitive = iParentFromCollider.GetParentTransform().GetComponent<IBlackHoleSensitive>();
        if ( blackHoleSensitive == null)
        {
            return;
        }

        blackHoleSensitive.DieFromBlackHole( _transform );
    }
}
