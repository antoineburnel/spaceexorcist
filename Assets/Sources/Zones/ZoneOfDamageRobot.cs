using UnityEngine;
using Ship;

public class ZoneOfDamageRobot : ZoneOfDamage
{
    public override void OnTriggerEnter2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship spaceship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null)
        {
            return;
        }

        AddSpaceship(spaceship);
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship spaceship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null)
        {
            return;
        }

        RemoveSpaceship(spaceship);
    }
}