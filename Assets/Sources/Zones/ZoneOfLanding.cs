using UnityEngine;
using Ship;
using System.Collections;
using Utils;

public class ZoneOfLanding : MonoBehaviour, IEventThatCanBeTriggered
{
    [SerializeField] private Spaceship _landedSpaceship = null;
    [SerializeField] private Transform _landingPos;
    [SerializeField] private Transform _takeOffPos;
    [SerializeField] private float _turningDuration = 0.5f;
    [SerializeField] private Spaceship _mothership;
    private float _landingDuration = 1.5f;
    private float _takingOffDuration = 1f;
    private float _initialSpeedAfterTakeOff;
    private static float INITIAL_SPEED_IF_NOT_MOVING_DURING_LANDING = 5f;
    private Transform _transform;
    [SerializeField] private float _zFinalRotation = 90f;
    private Transform _platformSprite;

    private void Awake()
    {
        _transform = transform;
        _platformSprite = _transform.GetChild(0).transform;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
        if (parent == null) return;

        Spaceship spaceship = parent.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null) return;

        spaceship.SetMoveE(this);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
        if (parent == null) return;

        Spaceship spaceship = parent.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null) return;

        spaceship.UnsetMoveE();
    }

    public void Trigger(Spaceship spaceship)
    {
        if (_landedSpaceship == null)
        {
            Land(spaceship);
        }
        else
        {
            TakeOff(spaceship);
        }
    }

    private void Land(Spaceship spaceship)
    {
        _landedSpaceship = spaceship;
        spaceship.UnsetMoveE();
        spaceship.SetUndamagable();
        spaceship.Land();
        spaceship.ResetVelocity();
        spaceship.StartCoroutine(LandAnimation(spaceship));
    }

    private void TakeOff(Spaceship spaceship)
    {
        _landedSpaceship = null;
        spaceship.UnsetMoveE();
        spaceship.TakeOff();
        spaceship.ResetVelocity();
        spaceship.StartCoroutine(TakingOffAnimation(spaceship));
    }

    private IEnumerator LandAnimation(Spaceship spaceship)
    {
        if (spaceship == null) yield return null;
        _landingDuration = (_landingPos.position - spaceship.transform.position).magnitude / 7.5f + 0.2f;

        Transform spaceshipTransform = spaceship.transform;
        Vector3 originalPosition = spaceshipTransform.position;
        Quaternion originalRotation = spaceshipTransform.rotation;
        Quaternion finalRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f)) * _landingPos.rotation;
        float originalSpeed = spaceship.GetSpeed();

        float elapsed = 0f;
        float t = 0f;

        if (originalSpeed == 0f)
        {

            while (elapsed < 0.1f)
            {

                elapsed += Time.unscaledDeltaTime;
                t = elapsed / 0.1f;
                spaceship.SetSpeed(Mathf.Lerp(0f, INITIAL_SPEED_IF_NOT_MOVING_DURING_LANDING, SmoothFunction.EaseInSquare(t)));

                yield return null;
            }
            originalSpeed = INITIAL_SPEED_IF_NOT_MOVING_DURING_LANDING;
        }

        elapsed = 0f;
        t = 0f;

        while (elapsed < _landingDuration)
        {

            elapsed += Time.unscaledDeltaTime;
            t = elapsed / _landingDuration;

            spaceshipTransform.position = Vector3.Lerp(originalPosition, _landingPos.position, SmoothFunction.EaseInOutSquare(t));
            spaceshipTransform.rotation = Quaternion.Lerp(originalRotation, finalRotation, t);
            spaceship.SetSpeed(Mathf.Lerp(originalSpeed, 0f, SmoothFunction.EaseInOutSquare(t)));

            yield return null;
        }

        spaceship.SetSpeed(0f);
        spaceship.ResetVelocity();
        spaceshipTransform.position = _landingPos.position;
        originalRotation = spaceshipTransform.rotation;
        spaceshipTransform.parent = _transform;

        spaceshipTransform.rotation = finalRotation;
        finalRotation = Quaternion.Euler(new Vector3(0f, 0f, _zFinalRotation)) * _landingPos.rotation;

        elapsed = 0f;
        t = 0f;

        while (elapsed < _turningDuration)
        {

            elapsed += Time.unscaledDeltaTime;
            t = elapsed / _turningDuration;

            spaceshipTransform.rotation = Quaternion.Lerp(originalRotation, finalRotation, SmoothFunction.EaseInOutSquare(t));
            _platformSprite.rotation = Quaternion.Lerp(originalRotation, finalRotation, SmoothFunction.EaseInOutSquare(t));

            yield return null;
        }

        spaceshipTransform.rotation = finalRotation;

        spaceship.SetMoveA(new SwitchControlFromShipToShip("Control mothership")
            .From(spaceship)
            .To(_mothership)
            .MustBeImmobile(true)
            .SwitchDuration(1f)
            .DisableColliders(true)
            .Bidirectional("Go back")
        );

        spaceship.SetMoveE(this);
        spaceship.HasLanded();
    }

    private IEnumerator TakingOffAnimation(Spaceship spaceship)
    {
        if (spaceship == null) yield return null;
        Transform spaceshipTransform = spaceship.transform;
        Vector3 originalPosition = spaceshipTransform.position;
        _initialSpeedAfterTakeOff = spaceship.GetMaxSpeed();
        spaceship.UnsetMoveA();
        _takingOffDuration = _initialSpeedAfterTakeOff / 10f;

        float elapsed = 0f;
        float t = 0f;

        while (elapsed < _takingOffDuration)
        {

            elapsed += Time.unscaledDeltaTime;
            t = elapsed / _takingOffDuration;

            spaceshipTransform.position = Vector3.Lerp(originalPosition, _takeOffPos.position, SmoothFunction.EaseInSquare(t));
            spaceship.SetSpeed(Mathf.Lerp(0f, _initialSpeedAfterTakeOff, SmoothFunction.EaseInSquare(t)));

            yield return null;
        }

        spaceshipTransform.position = _takeOffPos.position;
        spaceship.SetSpeed(_initialSpeedAfterTakeOff);
        spaceship.SetVelocity(new Vector2(0f, -_initialSpeedAfterTakeOff));

        spaceship.SetDamagable();
        spaceship.HasTookOff();
    }

    public string GetName(Spaceship spaceship)
    {
        if (_landedSpaceship == null)
        {
            return "Land";
        }
        return "Take-Off";
    }
}