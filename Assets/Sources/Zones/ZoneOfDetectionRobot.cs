using UnityEngine;
using Ship;

public class ZoneOfDetectionRobot : ZoneOfDetection
{

    public override void OnTriggerEnter2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship ship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (ship == null || ship.IsRobot())
        {
            return;
        }

        NotifyEnter(ship);
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship ship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (ship == null || ship.IsRobot())
        {
            return;
        }

        NotifyExit(ship);
    }
}