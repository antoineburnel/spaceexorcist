using UnityEngine;

public class ZoneOfDeath : MonoBehaviour
{
    private Transform _transform;

    private void Awake ()
    {
        _transform = transform;
    }

    private void OnTriggerEnter2D ( Collider2D other )
    {
        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if ( iParentFromCollider == null )
        {
            return;
        }

        ICanDie iCanDie = iParentFromCollider.GetParentTransform().GetComponent<ICanDie>();
        if ( iCanDie == null )
        {
            return;
        }

        iCanDie.Die( _transform );
    }
}
