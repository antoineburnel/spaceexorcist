using UnityEngine;
using System.Collections;
using Ship;
using Utils;
using System;

public class ZoneOfEating : MonoBehaviour, IZoneObserver<Spaceship>
{
    private Transform _transform;
    private Spaceship _target;
    private BoxCollider2D _collider;
    private static float DROP_DISTANCE = 2f;
    private static float DROP_EGG_DURATION = 0.5f;
    private Vector3 _initialPlayerLocalScale;
    [SerializeField] private Animator _eatingAnimator;
    [SerializeField] private float _timeToDieSeconds = 0.5f;
    [SerializeField] private Transform _startPosAnim;
    [SerializeField] private Transform _endPosAnim;
    [SerializeField] private GameObject _eggPrefab;
    private Vector3 _initialEggPosition;
    private Coroutine _eatCoroutine;
    private Coroutine _eggCoroutine;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _transform = transform;
        _target = null;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_target != null) return;

        GetParentFromCollider iParentFromCollider = other.GetComponent<GetParentFromCollider>();
        if (iParentFromCollider == null)
        {
            return;
        }

        Spaceship spaceship = iParentFromCollider.GetParentTransform().GetComponent<Spaceship>();
        if (spaceship == null || spaceship.IsAlien() || !spaceship.IsEatable())
        {
            return;
        }

        _target = spaceship;
        _initialPlayerLocalScale = _target.transform.localScale;
        spaceship.Register(this);
        spaceship.SetGettingEat();
        spaceship.SetUndamagable();
        _eatCoroutine = StartCoroutine(EatShipAnim(spaceship));
        _eggCoroutine = StartCoroutine(DropAlienEgg());
    }

    private IEnumerator EatShipAnim(Spaceship spaceship)
    {
        float elapsed = 0f;
        float t = 0f;

        _eatingAnimator.SetTrigger("eat");
        Transform spaceshipTransform = spaceship.GetComponent<Transform>();
        Vector3 startLocalScale = spaceshipTransform.localScale;
        Vector3 finalLocalScale = new Vector3(0.5f, 0.5f, 1f);

        while (elapsed < _timeToDieSeconds)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / _timeToDieSeconds;

            spaceshipTransform.position = Vector3.Lerp(_startPosAnim.position, _endPosAnim.position, SmoothFunction.EaseInSquare(t));
            spaceshipTransform.localScale = Vector3.Lerp(startLocalScale, finalLocalScale, SmoothFunction.EaseInSquare(t));
            yield return null;
        }

        _target = null;
        spaceship.Unregister(this);
        spaceshipTransform.position = _endPosAnim.position;
        spaceship.Die(_transform);
    }

    private IEnumerator DropAlienEgg()
    {
        yield return new WaitForSeconds(_timeToDieSeconds);
        Transform egg = Instantiate(_eggPrefab, transform.position, Quaternion.identity, ParentSingleton.GetInstance().GetShipParent()).transform;
        Vector3 offset = new Vector2(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized * DROP_DISTANCE;
        float elapsed = 0f;
        float t = 0f;
        Vector3 initialEggPosition = egg.position;
        Vector3 finalEggPosition = initialEggPosition + offset;
        while (elapsed < DROP_EGG_DURATION)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / DROP_EGG_DURATION;
            egg.position = Vector3.Lerp(initialEggPosition, finalEggPosition, SmoothFunction.EaseOutCubic(t));
            yield return null;
        }
        egg.position = finalEggPosition;
        egg.GetComponent<SpriteRenderer>().sortingOrder = 150;
    }

    public void StopEatingAnimation()
    {
        StopAllCoroutines();
    }

    public void OnDeath(Spaceship subject)
    {
        StopEatingAnimation();
        _target = null;
    }

    private void OnDestroy()
    {
        if (_target == null) return;
        _target.Unregister(this);
        StopEatingAnimation();
        _target.SetDamagable();
        _target.UnsetGettingEat();
        _target.transform.localScale = _initialPlayerLocalScale;
    }
}