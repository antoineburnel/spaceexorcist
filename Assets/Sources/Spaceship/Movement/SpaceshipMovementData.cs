using UnityEngine;

public class SpaceshipMovementData
{
    private Vector2 _movementBuffer;
    private Vector2 _movement;

    public SpaceshipMovementData() {
        _movementBuffer = new Vector2();
        _movement = new Vector2();
    }

    public void UpdateMovement() {

        _movementBuffer.x = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.W)) _movementBuffer.y = 1f;
        else _movementBuffer.y = -1f;
        _movementBuffer.Normalize();
        _movement.x = _movementBuffer.x;
        _movement.y = _movementBuffer.y;
    }

    public Vector2 GetMovementVector() {
        return _movement;
    }
}