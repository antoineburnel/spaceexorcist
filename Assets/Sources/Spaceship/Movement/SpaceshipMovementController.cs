using UnityEngine;

public interface SpaceshipMovementController
{
    public void UpdateMovementData();
    public Vector2 GetSpaceshipMovementData();
    public void DoUpdate();
    public void ResetVelocity();
    public void SetVelocity(Vector2 newVelocity);
}