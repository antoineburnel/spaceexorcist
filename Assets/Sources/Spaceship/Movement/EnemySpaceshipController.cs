namespace Ship.Movement
{
    using UnityEngine;

    public class EnemySpaceshipController : MonoBehaviour, SpaceshipMovementController
    {
        protected Rigidbody2D _rb;
        protected SpaceshipData _data;
        protected Transform _transform;
        private SpaceshipMovementData _movementData;
        private Vector2 _movementVector;
        private Vector2 _nextVelocity;
        private Vector3 _additiveRotation;
        private float _currentSpeed;
        private RobotSpaceshipAI _ai;

        public void Init(Rigidbody2D rb, SpaceshipData data, Transform transform, RobotSpaceshipAI ai)
        {
            _rb = rb;
            _data = data;
            _transform = transform;
            _nextVelocity = new Vector2();
            _additiveRotation = new Vector3();
            _currentSpeed = 0;
            _ai = ai;
        }

        public void UpdateMovementData()
        {

        }

        public Vector2 GetSpaceshipMovementData()
        {
            return _ai.GetNextPositionInput();
        }

        public void DoUpdate()
        {
            this.UpdateMovementData();
            _movementVector = this.GetSpaceshipMovementData();
            this.ApplyMovement();
            this.ApplyRotation();
        }

        private void ApplyMovement() {
            if (_movementVector.y > 0)
            {
                _currentSpeed += _movementVector.y * _data.GetAcceleration();
            }
            else
            {
                _currentSpeed += _movementVector.y * _data.GetDeceleration();
                if (_currentSpeed < 0)
                {
                    _currentSpeed = 0;
                }
            }
            if (_currentSpeed > _data.GetMaxSpeed())
            {
                _currentSpeed = _data.GetMaxSpeed();
            }
            else if (_currentSpeed < -_data.GetMaxSpeed())
            {
                _currentSpeed = -_data.GetMaxSpeed();
            }
            _data.SetSpeed(_currentSpeed);
            _nextVelocity = _transform.right * _currentSpeed;
            if (_nextVelocity != _rb.velocity)
            {
                _rb.velocity = _nextVelocity;
            }
        }

        private void ApplyRotation() {
            float rotationAmount = -_movementVector.x * _data.GetTurningSpeed();
            _additiveRotation.z = rotationAmount;
            _transform.Rotate(_additiveRotation, Space.Self);
        }
    
        public void ResetVelocity ()
        {
            _rb.velocity = Vector2.zero;
            _movementVector = Vector2.zero;
            _nextVelocity = Vector2.zero;
            _currentSpeed = 0f;
        }

        public void SetVelocity ( Vector2 newVelocity )
        {
            _rb.velocity = Vector2.zero;
            _movementVector = newVelocity;
            _nextVelocity = newVelocity;
            _currentSpeed = newVelocity.magnitude;
        }
    }
}