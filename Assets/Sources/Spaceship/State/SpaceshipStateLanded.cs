public class SpaceshipStateLanded : SpaceshipState
{
    public override bool CanFire()
    {
        return true;
    }
    
    public override bool CanTakeOff()
    {
        return true;
    }
    
    public override bool IsLanded()
    {
        return true;
    }

    public override SpaceshipState TakeOff()
    {
        return new SpaceshipStateTakingOff();
    }
}
