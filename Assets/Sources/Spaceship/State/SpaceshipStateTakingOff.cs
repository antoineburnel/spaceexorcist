public class SpaceshipStateTakingOff : SpaceshipState
{
    public override bool CanFire()
    {
        return true;
    }
    
    public override bool IsLanded()
    {
        return false;
    }

    public override bool IsTakingOff()
    {
        return true;
    }

    public override SpaceshipState HasTookOff()
    {
        return new SpaceshipStateFlying();
    }
}
