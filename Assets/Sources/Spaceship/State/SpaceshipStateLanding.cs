public class SpaceshipStateLanding : SpaceshipState
{
    public override bool CanFire()
    {
        return true;
    }
    
    public override bool IsLanding()
    {
        return true;
    }

    public override SpaceshipState HasLanded()
    {
        return new SpaceshipStateLanded();
    }
}
