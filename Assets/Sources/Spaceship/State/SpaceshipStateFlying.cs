public class SpaceshipStateFlying : SpaceshipState
{
    public override bool CanFire()
    {
        return true;
    }

    public override bool CanMove()
    {
        return true;
    }

    public override bool IsFlying()
    {
        return true;
    }

    public override SpaceshipState Land()
    {
        return new SpaceshipStateLanding();
    }
}
