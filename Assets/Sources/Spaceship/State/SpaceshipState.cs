public abstract class SpaceshipState
{
    public virtual bool IsLanding() {return false;}
    public virtual bool IsLanded() {return false;}
    public virtual bool IsFlying() {return false;}
    public virtual bool IsTakingOff() {return false;}
    public virtual bool CanFire() {return false;}
    public virtual bool CanMove() {return false;}
    public virtual bool CanTakeOff() {return false;}
    public virtual SpaceshipState TakeOff() {return this;}
    public virtual SpaceshipState Land() {return this;}
    public virtual SpaceshipState HasLanded() {return this;}
    public virtual SpaceshipState HasTookOff() {return this;}
}
