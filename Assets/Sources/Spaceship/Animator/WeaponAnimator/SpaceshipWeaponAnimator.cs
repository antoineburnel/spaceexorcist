namespace Ship.Animation
{
    public interface SpaceshipWeaponAnimator
    {
        public void Init();
        public void DoUpdate();
    }
}