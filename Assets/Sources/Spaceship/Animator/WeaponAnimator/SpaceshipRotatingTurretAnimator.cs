namespace Ship.Animation
{
    using UnityEngine;

    public class SpaceshipRotatingTurretAnimator : MonoBehaviour, SpaceshipWeaponAnimator
    {
        private Transform _turretTransform;
        private Camera _camera;

        public void Init()
        {
            _turretTransform = transform.GetChild(0).GetChild(4).GetChild(1);
            _camera = Camera.main;
        }

        public void DoUpdate()
        {
            Vector3 mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 direction = mousePos - _turretTransform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            _turretTransform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}