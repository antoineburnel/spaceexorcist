namespace Ship.Animation
{
    using UnityEngine;
    using Utils;

    public class SpaceshipWingsAnimator : MonoBehaviour
    {

        private Transform _transform;
        private float _stretchIncrease;
        private float _diffFinalPosition;
        private float _diffFinalScale;

        private Transform _leftWing;
        private Vector3 _leftWingBasePosition;
        private Vector3 _leftWingFinalPosition;
        private Vector3 _leftWingBaseScale;
        private Vector3 _leftWingFinalScale;
        private float _stretchLeft;

        private Transform _rightWing;
        private Vector3 _rightWingBasePosition;
        private Vector3 _rightWingFinalPosition;
        private Vector3 _rightWingBaseScale;
        private Vector3 _rightWingFinalScale;
        private float _stretchRight;

        public void Init()
        {
            _transform = transform;

            _leftWing = _transform.GetChild(0).GetChild(2);
            _leftWingBasePosition = _leftWing.localPosition;
            _leftWingBaseScale = _leftWing.localScale;

            _rightWing = _transform.GetChild(0).GetChild(3);
            _rightWingBasePosition = _rightWing.localPosition;
            _rightWingBaseScale = _rightWing.localScale;

            _stretchIncrease = 0.02f;
            _diffFinalPosition = 0.06f;
            _diffFinalScale = 0.75f;

            _rightWingFinalPosition = _rightWingBasePosition + new Vector3(0, _diffFinalPosition, 0);
            _rightWingFinalScale = _rightWingBaseScale + new Vector3(0, -_diffFinalScale, 0);
            _leftWingFinalPosition = _leftWingBasePosition - new Vector3(0, _diffFinalPosition, 0);
            _leftWingFinalScale = _leftWingBaseScale + new Vector3(0, -_diffFinalScale, 0);
        }

        public void DoUpdate(Vector3 movementVector)
        {
            ComputeStretchFactor(movementVector);
            AnimateLeftWing();
            AnimateRightWing();
        }

        private void ComputeStretchFactor(Vector3 movementVector)
        {
            if (movementVector.x > 0)
            {
                _stretchLeft -= _stretchIncrease;
                _stretchRight += _stretchIncrease;
            }
            else if (movementVector.x < 0)
            {
                _stretchLeft += _stretchIncrease;
                _stretchRight -= _stretchIncrease;
            }
            else
            {
                _stretchLeft -= _stretchIncrease;
                _stretchRight -= _stretchIncrease;
            }
            _stretchLeft = Mathf.Clamp(_stretchLeft, 0f, 1f);
            _stretchRight = Mathf.Clamp(_stretchRight, 0f, 1f);
        }

        private void AnimateLeftWing()
        {
            _leftWing.localPosition = Vector3.Lerp(_leftWingBasePosition, _leftWingFinalPosition, SmoothFunction.EaseInOutSquare(_stretchLeft));
            _leftWing.localScale = Vector3.Lerp(_leftWingBaseScale, _leftWingFinalScale, SmoothFunction.EaseInOutSquare(_stretchLeft));
        }

        private void AnimateRightWing()
        {
            _rightWing.localPosition = Vector3.Lerp(_rightWingBasePosition, _rightWingFinalPosition, SmoothFunction.EaseInOutSquare(_stretchRight));
            _rightWing.localScale = Vector3.Lerp(_rightWingBaseScale, _rightWingFinalScale, SmoothFunction.EaseInOutSquare(_stretchRight));
        }
    }
}