namespace Ship.Animation
{
    using UnityEngine;

    public class SpaceshipBoostAnimation : MonoBehaviour
    {
        private Transform _transform;
        private ParticleSystem[] _psBoosts;
        private ParticleSystem.EmissionModule[] _psBoostsFireEmission;
        private ParticleSystem.ColorOverLifetimeModule[] _psBoostsFireColor;
        private float _speedToQuantity;
        private Color _colorFire;
        private Color _colorMin;
        private Color _colorMax;
        private float _randomIntensity;
        private float _speedRatio;
        private float _maxSpeed;
        private SpaceshipData _data;

        public void Init(SpaceshipData data)
        {
            _transform = transform;
            _data = data;

            int nbBoosts = _transform.GetChild(0).GetChild(1).childCount;
            _psBoosts = new ParticleSystem[nbBoosts];
            _psBoostsFireEmission = new ParticleSystem.EmissionModule[nbBoosts];
            _psBoostsFireColor = new ParticleSystem.ColorOverLifetimeModule[nbBoosts];

            for (int i = 0; i < nbBoosts; i++)
            {
                _psBoosts[i] = _transform.GetChild(0).GetChild(1).GetChild(i).GetComponent<ParticleSystem>();
                _psBoostsFireEmission[i] = _psBoosts[i].emission;
                _psBoostsFireColor[i] = _psBoosts[i].colorOverLifetime;
            }

            _colorMin = _data.GetMinColorBoost();
            _colorMax = _data.GetMaxColorBoost();
            _maxSpeed = _data.GetMaxSpeed();

            _speedToQuantity = 10;
        }

        public void DoUpdate()
        {
            _speedRatio = _data.GetSpeed() / _maxSpeed;
            _randomIntensity = Random.Range(0.8f, 1.2f);
            _colorFire = Color.Lerp(_colorMin, _colorMax, _speedRatio) * _randomIntensity;

            for (int i = 0; i < _psBoosts.Length; i++)
            {
                _psBoostsFireColor[i].color = _colorFire;
                _psBoostsFireEmission[i].rateOverTime = _speedToQuantity * _data.GetSpeed();
            }
        }
    }
}