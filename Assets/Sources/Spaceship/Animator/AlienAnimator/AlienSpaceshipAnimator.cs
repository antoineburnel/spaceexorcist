namespace Ship.Animation
{
    using UnityEngine;

    [RequireComponent(typeof(AlienBoostAnimator))]
    public class AlienSpaceshipAnimator : MonoBehaviour, SpaceshipAnimator {

        private AlienBoostAnimator _alienBoostAnimator;

        public void Init (SpaceshipData data) {
            _alienBoostAnimator = GetComponent<AlienBoostAnimator>();
            _alienBoostAnimator.Init();
        }

        public void DoUpdate( SpaceshipData data, Vector2 movementData ) {
            _alienBoostAnimator.DoUpdate();
        }
    }
}