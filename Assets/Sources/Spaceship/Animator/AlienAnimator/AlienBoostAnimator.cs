namespace Ship.Animation
{
    using Ship;
    using UnityEngine;

    public class AlienBoostAnimator : MonoBehaviour
    {
        private Animator _boostAnimator;
        private SpaceshipData _data;

        public void Init()
        {
            _boostAnimator = transform.GetChild(0).GetChild(2).GetComponent<Animator>();
            _data = GetComponent<Spaceship>().GetData();
        }

        public void DoUpdate()
        {
            _boostAnimator.SetFloat("speed", _data.GetSpeed());
        }
    }
}