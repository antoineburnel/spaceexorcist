namespace Ship.Animation
{
    using UnityEngine;

    [RequireComponent(typeof(SpaceshipBoostAnimation))]
    [RequireComponent(typeof(SpaceshipWingsAnimator))]
    [RequireComponent(typeof(SpaceshipWeaponAnimator))]
    public class EnemySpaceshipAnimator : MonoBehaviour, SpaceshipAnimator {

        private Transform _transform;
        private SpaceshipBoostAnimation _spaceshipBoostAnimator;
        private SpaceshipWingsAnimator _spaceshipWingsAnimator;
        private SpaceshipWeaponAnimator _spaceshipWeaponAnimator;

        public void Init(SpaceshipData data)
        {
            _transform = transform;
            _spaceshipBoostAnimator = _transform.GetComponent<SpaceshipBoostAnimation>();
            _spaceshipWingsAnimator = _transform.GetComponent<SpaceshipWingsAnimator>();
            _spaceshipWeaponAnimator = _transform.GetComponent<SpaceshipWeaponAnimator>();
            _spaceshipBoostAnimator.Init(data);
            _spaceshipWingsAnimator.Init();
            _spaceshipWeaponAnimator.Init();
        }

        public void DoUpdate( SpaceshipData data, Vector2 movementData )
        {
            _spaceshipBoostAnimator.DoUpdate();
            _spaceshipWingsAnimator.DoUpdate( movementData );
            _spaceshipWeaponAnimator.DoUpdate();
        }
    }
}