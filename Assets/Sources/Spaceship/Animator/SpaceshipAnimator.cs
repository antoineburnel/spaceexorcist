namespace Ship.Animation
{
    using UnityEngine;

    public interface SpaceshipAnimator
    {
        public void Init(SpaceshipData data);
        public void DoUpdate(SpaceshipData data, Vector2 movementData);
    }
}