using Ship;
using UnityEngine;

public class ControlledByRobot : MonoBehaviour
{

    private RobotSpaceship _ship;
    [SerializeField] private bool _enabled = true;
    [SerializeField] private bool _update = true;
    [SerializeField] private bool _fixedUpdate = true;

    private void Awake()
    {
        _ship = gameObject.GetComponent<RobotSpaceship>();
    }

    private void Start()
    {
        _ship.Init();
        if (!_enabled) this.enabled = false;
    }

    private void Update()
    {
        if (!_update) return;
        _ship.DoUpdate();
    }

    private void FixedUpdate()
    {
        if (!_fixedUpdate) return;
        _ship.DoFixedUpdate();
    }
}