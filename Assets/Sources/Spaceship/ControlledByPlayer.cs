using Ship;
using UnityEngine;

public class ControlledByPlayer : MonoBehaviour
{

    private PlayerSpaceship _ship;
    [SerializeField] private bool _enabled = false;

    private void Awake()
    {
        _ship = gameObject.GetComponent<PlayerSpaceship>();
    }

    private void Start()
    {
        _ship.Init();
        if (!_enabled) this.enabled = false;
        else _ship.UpdateUI();
    }

    private void OnEnable()
    {
        _ship.ChangeAllPlayerLayerToControlledPlayerLayer();
    }

    private void OnDisable()
    {
        _ship.ChangeAllControlledPlayerLayerToPlayerLayer();
    }

    private void Update()
    {
        _ship.DoUpdate();
    }

    private void FixedUpdate()
    {
        _ship.DoFixedUpdate();
    }

    private void LateUpdate()
    {
        _ship.DoLateUpdate();
    }
}