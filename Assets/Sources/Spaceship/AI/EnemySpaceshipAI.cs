using UnityEngine;
using Ship;
using System.Collections.Generic;

public abstract class EnemySpaceshipAI : MonoBehaviour, IDetectionManager, ISpaceshipEvents
{
    protected EnemySpaceshipAIState _state;
    private Transform _target;
    private ZoneOfDetection _zoneOfDetection;
    private Transform _transform;
    protected Spaceship _spaceship;
    protected float _turnSpeed;
    private SpaceshipReactionAnimator _reactionAnimator;
    private SpaceshipLightAnimator _lightAnimator;

    private Sprite _searchSprite;
    private Sprite _chaseSprite;
    private Color _white;
    private Color _orange;
    private Color _red;

    protected List<EnemySpaceshipAIState> _statePool;

    private void Awake()
    {
        _statePool = new List<EnemySpaceshipAIState>();    
    }

    public void Init(Spaceship spaceship)
    {
        _searchSprite = Resources.Load<Sprite>("AI/Searching");
        _chaseSprite = Resources.Load<Sprite>("AI/Chasing");

        _white = new Color32(255, 255, 255, 50);
        _orange = new Color32(255, 112, 0, 50);
        _red = new Color32(255, 0, 0, 50);

        AddAItoAIS(this);
        _transform = transform;
        _spaceship = spaceship;
        _target = null;
        _turnSpeed = _spaceship.GetTurnSpeed();
        _lightAnimator = _transform.GetChild(2).GetChild(1).GetComponent<SpaceshipLightAnimator>();
        _reactionAnimator = _transform.GetChild(2).GetChild(0).GetComponent<SpaceshipReactionAnimator>();
        InitStatePool();
        SetStatePatrol();
    }

    public abstract void AddAItoAIS(EnemySpaceshipAI ai);

    public abstract void InitStatePool();

    public void SetStatePatrol()
    {
        SetLightColor(_white);
        SetReactionSprite(null);
        _state = _statePool[0];
        _state.EVENT_OnChange(_target);
    }

    public void SetStateSearch()
    {
        SetLightColor(_orange);
        SetReactionSprite(_searchSprite);
        _state = _statePool[1];
        _state.EVENT_OnChange(_target);
    }

    public void SetStateGoToAlert(Transform target)
    {
        if (target == null) return;
        SetLightColor(_orange);
        SetReactionSprite(_searchSprite);
        _state = _statePool[2];
        _target = target;
        _state.EVENT_OnChange(_target);
    }

    public void SetStateChase(Transform target)
    {
        if (target == null) return;
        SetLightColor(_red);
        SetReactionSprite(_chaseSprite);
        _state = _statePool[3];
        _target = target;
        _state.EVENT_OnChange(_target);
    }

    public EnemySpaceshipAIState GetState()
    {
        return _state;
    }

    public Spaceship GetSpaceship()
    {
        return _spaceship;
    }

    public void ChangeTarget(Transform newTarget)
    {
        _target = newTarget;
    }

    public Transform GetTransform()
    {
        return _transform;
    }

    public float GetTurnSpeed()
    {
        return _turnSpeed;
    }

    public bool IsTargeted(Transform target)
    {
        return _target == target;
    }

    private void SetReactionSprite(Sprite reaction)
    {
        _reactionAnimator.SetReaction(reaction);
    }

    private void SetLightColor(Color color)
    {
        _lightAnimator.SetLight(color);
    }

    public void EVENT_OnEnterDetectionZone(Transform target)
    {
        _state.EVENT_OnEnterDetectionZone(target);
    }

    public void EVENT_OnExitDetectionZone(Transform target)
    {
        _state.EVENT_OnExitDetectionZone(target);
    }

    public void EVENT_OnReceivedDamage(Transform target)
    {
        _state.EVENT_OnReceivedDamage(target);
    }

    public Vector2 GetNextPositionInput()
    {
        return _state.GetNextPositionInput(_target);
    }

    protected static void BROADCAST_DeathOfAlly(ICollection<EnemySpaceshipAI> AIs, Transform target)
    {
        foreach (EnemySpaceshipAI AI in AIs)
        {
            AI.GetState().EVENT_DeathOfAlly(target);
        }
    }

    protected static void BROADCAST_DeathOfEnemy(ICollection<EnemySpaceshipAI> AIs, Transform target)
    {
        foreach (EnemySpaceshipAI AI in AIs)
        {
            AI.GetState().EVENT_DeathOfEnemy(target);
        }
    }

    protected static void BROADCAST_EnemyFoundAtPosition(ICollection<EnemySpaceshipAI> AIs, Transform target)
    {
        foreach (EnemySpaceshipAI AI in AIs)
        {
            AI.GetState().EVENT_PlayerFoundAtPosition(target);
        }
    }

    public static void EVENT_DeclareDeath(Spaceship target)
    {
        AlienSpaceshipAI.OnDeath(target);
        RobotSpaceshipAI.OnDeath(target);
    }

    public void EVENT_OnDeath(Transform target)
    {
    }

    private void OnDestroy()
    {
        for (int i = _statePool.Count-1; i >= 0; i--) {
            _statePool.RemoveAt(i);
        }
        Resources.UnloadAsset(_searchSprite);
        Resources.UnloadAsset(_chaseSprite);
    }
}