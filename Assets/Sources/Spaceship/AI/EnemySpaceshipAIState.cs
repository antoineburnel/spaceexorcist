using UnityEngine;

public abstract class EnemySpaceshipAIState : IDetectionManager
{
    protected Transform _transform;
    protected Vector2 _input;
    protected EnemySpaceshipAI _ai;

    public EnemySpaceshipAIState(Transform transform, EnemySpaceshipAI ai)
    {
        _transform = transform;
        _input = new Vector2();
        _ai = ai;
    }

    public abstract string GetStateName();

    public abstract Vector2 GetNextPositionInput(Transform target);
    
    public abstract void EVENT_OnEnterDetectionZone(Transform target);

    public abstract void EVENT_OnExitDetectionZone(Transform target);

    public abstract void EVENT_PlayerFoundAtPosition(Transform target);

    public abstract void EVENT_OnReceivedDamage(Transform shooterTransform);

    public abstract void EVENT_DeathOfAlly(Transform position);

    public abstract void EVENT_DeathOfEnemy(Transform target);

    public void EVENT_OnDeath(Transform target)
    {
        _ai.EVENT_OnDeath(target);
    }

    public virtual void EVENT_OnChange(Transform target)
    {
    }
}
