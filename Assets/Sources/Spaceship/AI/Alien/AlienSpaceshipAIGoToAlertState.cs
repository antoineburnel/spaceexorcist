using UnityEngine;
using Utils;

public class AlienSpaceshipAIGoToAlertState : AlienSpaceshipAIState
{
    private float _visionRange;
    private const float BIGGEST_DISTANCE_FOR_ONE_AXE = 50f;
    private bool _isYellow;
    private Vector3 _targetPosition;
    private const float MINIMUM_DELTA_TO_MOVE = 20f;

    public AlienSpaceshipAIGoToAlertState(AlienSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
        _visionRange = ai.GetSpaceship().GetVisionRange() / 10f;
    }

    public override void EVENT_OnChange(Transform target)
    {
        _targetPosition = target.position;
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.SetStateChase(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {
    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
    }

    public override string GetStateName()
    {
        return "GOTOALERT";
    }

    public override Vector2 GetNextPositionInput(Transform _target)
    {
        float distanceToGoal = Heuristics.euclideanDistance(_transform.position, _targetPosition);
        if (distanceToGoal <= _visionRange) {
            _ai.SetStatePatrol();
            _input.Set(0, 0);
            return _input;
        }

        Vector3 direction = _targetPosition - _transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        float currentAngle = _transform.rotation.eulerAngles.z;
        float delta = Mathf.DeltaAngle(angle, currentAngle);

        float turnSpeed = _ai.GetTurnSpeed();
        float inputX = Mathf.Clamp(delta / turnSpeed, -1f, 1f);

        if (Mathf.Abs(delta) > MINIMUM_DELTA_TO_MOVE) {
            if (_ai.GetSpaceship().GetSpeed() > 0f) {
                _input.Set(0, -1f);
                return _input;
            }
            _input.Set(inputX, 0f);
            return _input;
        }

        _input.Set(inputX, 1f);
        return _input;
    }

    public override void EVENT_OnReceivedDamage(Transform shooterTransform)
    {
        _targetPosition = shooterTransform.position;
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
        if (_ai.IsTargeted(target))
        {
            _ai.SetStatePatrol();
        }
    }
}
