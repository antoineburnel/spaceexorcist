using UnityEngine;

public class AlienSpaceshipAIChaseState : AlienSpaceshipAIState
{

    public AlienSpaceshipAIChaseState(AlienSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.ChangeTarget(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {

    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
    }

    public override string GetStateName()
    {
        return "CHASE";
    }

    public override Vector2 GetNextPositionInput(Transform target)
    {
        if (target == null) {
            _ai.SetStatePatrol();
            _input.Set(0f, 0f);
            return _input;
        }
        Vector3 direction = _transform.position - target.position;
        float targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        float currentAngle = _transform.rotation.eulerAngles.z;
        float delta = Mathf.DeltaAngle(currentAngle, targetAngle);
        float turnSpeed = _ai.GetTurnSpeed();
        float input = (delta / turnSpeed) % 1f;
        _input.Set(input, 1f);
        AlienSpaceshipAI.EVENT_PlayerFoundAtPosition(target);
        return _input;
    }

    public override void EVENT_OnReceivedDamage(Transform shooterTransform)
    {
        _ai.ChangeTarget(shooterTransform);
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
        if (_ai.IsTargeted(target))
        {
            _ai.SetStatePatrol();
        }
    }
}
