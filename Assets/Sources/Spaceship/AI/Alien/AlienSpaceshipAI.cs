using System.Collections.Generic;
using UnityEngine;
using Ship;

public class AlienSpaceshipAI : EnemySpaceshipAI
{
    private static List<EnemySpaceshipAI> AIs = new List<EnemySpaceshipAI>();

    public override void AddAItoAIS(EnemySpaceshipAI ai)
    {
        AIs.Add(ai);
    }

    public override void InitStatePool()
    {
        _statePool.Add(new AlienSpaceshipAIPatrolState(this, GetTransform()));
        _statePool.Add(new AlienSpaceshipAISearchAroundState(this, GetTransform()));
        _statePool.Add(new AlienSpaceshipAIGoToAlertState(this, GetTransform()));
        _statePool.Add(new AlienSpaceshipAIChaseState(this, GetTransform()));   
    }

    public static void OnDeath(Spaceship target)
    {
        if (target.IsAlien())
        {
            AIs.Remove(target.GetAI());
            BROADCAST_DeathOfAlly(AIs, target.transform);
            return;
        }
        BROADCAST_DeathOfEnemy(AIs, target.transform);
    }

    public static void EVENT_PlayerFoundAtPosition(Transform target)
    {
        EnemySpaceshipAI.BROADCAST_EnemyFoundAtPosition(AIs, target);
    }
}