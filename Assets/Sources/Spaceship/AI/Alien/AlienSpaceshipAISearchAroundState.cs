using UnityEngine;
using Ship;

public class AlienSpaceshipAISearchAroundState : AlienSpaceshipAIState
{
    private const float SEARCH_DURATION = 2.5f;
    private const float TURNING_SPEED = 1f;
    private float _elapsed;
    private Spaceship _spaceship;
    private float _turnSign;

    public AlienSpaceshipAISearchAroundState(AlienSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
        _elapsed = 0f;
        _spaceship = ai.GetSpaceship();
        _turnSign = Random.value < 0.5f ? -TURNING_SPEED : TURNING_SPEED;
    }
    
    public override Vector2 GetNextPositionInput(Transform _target)
    {
        _elapsed += Time.fixedDeltaTime;
        if (_elapsed > SEARCH_DURATION) {
            _ai.SetStatePatrol();
            _input.Set(0, 0);
            return _input;
        }
        float forwardSpeedToReduce = Mathf.Min(_spaceship.GetSpeed(), 1f);
        _input.Set(_turnSign, -forwardSpeedToReduce);
        return _input;
    }

    public override string GetStateName()
    {
        return "SEARCHING AROUND";
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.SetStateChase(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {
    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
    }

    public override void EVENT_OnReceivedDamage(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
    }
}
