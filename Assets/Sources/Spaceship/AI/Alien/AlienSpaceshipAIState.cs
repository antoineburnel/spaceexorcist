using UnityEngine;
public abstract class AlienSpaceshipAIState : EnemySpaceshipAIState
{
    public AlienSpaceshipAIState(AlienSpaceshipAI ai, Transform transform) : base(transform, ai)
    {
    }
}
