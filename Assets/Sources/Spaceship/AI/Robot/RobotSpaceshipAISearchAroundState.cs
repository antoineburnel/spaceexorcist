using UnityEngine;
using Ship;

public class RobotSpaceshipAISearchAroundState : RobotSpaceshipAIState
{
    private const float SEARCH_DURATION = 5f;
    private const float TURNING_SPEED = 1f;
    private float _elapsed;
    private Spaceship _spaceship;
    private float _turnSign;

    public RobotSpaceshipAISearchAroundState(RobotSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
        _elapsed = 0f;
        _spaceship = ai.GetSpaceship();
        _turnSign = Random.value < 0.5f ? -TURNING_SPEED : TURNING_SPEED;
    }

    public override void EVENT_OnChange(Transform target)
    {
        _elapsed = 0f;
    }
    
    public override Vector2 GetNextPositionInput(Transform _target)
    {
        _elapsed += Time.unscaledDeltaTime;
        if (_elapsed > SEARCH_DURATION) {
            _ai.SetStatePatrol();
            _input.Set(0f, 0f);
            return _input;
        }
        float forwardSpeedToReduce = Mathf.Min(_spaceship.GetSpeed(), 1f);
        _input.Set(_turnSign, -forwardSpeedToReduce);
        return _input;
    }

    public override string GetStateName()
    {
        return "SEARCHING AROUND";
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.SetStateChase(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {
    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }
    
    public override void EVENT_OnReceivedDamage(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
    }
}
