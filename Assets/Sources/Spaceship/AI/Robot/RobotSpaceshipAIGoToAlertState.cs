using UnityEngine;
using Utils;

public class RobotSpaceshipAIGoToAlertState : RobotSpaceshipAIState
{
    private float _visionRange;
    private const float BIGGEST_DISTANCE_FOR_ONE_AXE = 50f;
    private bool _isYellow;
    private Vector3 _targetPosition;
    private const float MINIMUM_DELTA_TO_MOVE = 20f;

    public RobotSpaceshipAIGoToAlertState(RobotSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
        _visionRange = ai.GetSpaceship().GetVisionRange() / 10f;
    }

    public override void EVENT_OnChange(Transform target)
    {
        if (target == null) return;
        _targetPosition = target.position;
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.SetStateChase(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {
    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
        _targetPosition = target.position;
    }

    public override string GetStateName()
    {
        return "GOTOALERT";
    }

    public override Vector2 GetNextPositionInput(Transform target)
    {
        if (target == null || Heuristics.euclideanDistance(_targetPosition, _transform.position) < _visionRange) {
            _ai.SetStatePatrol();
            _input.Set(0f, 0f);
            return _input;
        }
        Vector3 direction = _transform.position - _targetPosition;
        float targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        float currentAngle = _transform.rotation.eulerAngles.z;
        float delta = Mathf.DeltaAngle(currentAngle, targetAngle);
        float turnSpeed = _ai.GetTurnSpeed();
        float input = (delta / turnSpeed) % 1f;
        _input.Set(input, 1f);
        return _input;
    }

    public override void EVENT_OnReceivedDamage(Transform shooterTransform)
    {
        if (shooterTransform == null) return;
        _targetPosition = shooterTransform.position;
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
        if (_ai.IsTargeted(target))
        {
            _ai.SetStatePatrol();
        }
    }
}
