using UnityEngine;
using Utils;

public class RobotSpaceshipAIPatrolState : RobotSpaceshipAIState
{
    private float _visionRange;
    private Vector3 _nextPositionToReach;
    private const float BIGGEST_DISTANCE = 20f;
    private const float MINIMUM_DELTA_TO_MOVE = 20f;

    public RobotSpaceshipAIPatrolState(RobotSpaceshipAI ai, Transform transform) : base(ai, transform)
    {
        _visionRange = ai.GetSpaceship().GetVisionRange() / 10f;
    }

    public override void EVENT_OnChange(Transform target)
    {
        _nextPositionToReach = GenerateNewPoint(_transform.position);
    }

    public override void EVENT_OnEnterDetectionZone(Transform target)
    {
        _ai.SetStateChase(target);
    }

    public override void EVENT_OnExitDetectionZone(Transform target)
    {
    }

    public override void EVENT_PlayerFoundAtPosition(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }

    public override string GetStateName()
    {
        return "PATROL";
    }

    public override Vector2 GetNextPositionInput(Transform _target)
    {
        float distanceToGoal = Heuristics.euclideanDistance(_transform.position, _nextPositionToReach);
        if (distanceToGoal <= _visionRange) {
            _nextPositionToReach = GenerateNewPoint(_transform.position);
            _input.Set(0f, 0f);
            return _input;
        }

        Vector3 direction = _nextPositionToReach - _transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        float currentAngle = _transform.rotation.eulerAngles.z;
        float delta = Mathf.DeltaAngle(angle, currentAngle);

        float turnSpeed = _ai.GetTurnSpeed();
        float inputX = Mathf.Clamp(delta / turnSpeed, -1f, 1f);

        if (Mathf.Abs(delta) > MINIMUM_DELTA_TO_MOVE) {
            if (_ai.GetSpaceship().GetSpeed() > 0f) {
                _input.Set(0f, -1f);
                return _input;
            }
            _input.Set(inputX, 0f);
            return _input;
        }
        _input.Set(inputX, 1f);
        return _input;
    }

    private Vector3 GenerateNewPoint(Vector3 position)
    {
        int i = 0;
        Vector2 offset;
        do {
            if (i == 50) { Debug.LogError("Impossible to fastly generate the next point"); return Vector3.zero; }
            offset = Random.insideUnitCircle * BIGGEST_DISTANCE;
            i++;
        } while (!BasicPathfinding.CanGoFromPointAToPointB(position, position + (Vector3)offset));
        return position + (Vector3)offset;
    }

    public override void EVENT_OnReceivedDamage(Transform shooterTransform)
    {
        _ai.SetStateSearch();
    }

    public override void EVENT_DeathOfAlly(Transform target)
    {
        _ai.SetStateGoToAlert(target);
    }

    public override void EVENT_DeathOfEnemy(Transform target)
    {
    }
}
