using UnityEngine;
public abstract class RobotSpaceshipAIState : EnemySpaceshipAIState
{
    public RobotSpaceshipAIState(RobotSpaceshipAI ai, Transform transform) : base(transform, ai)
    {
    }
}
