using UnityEngine;

public class SpaceshipLight 
{
    private Color32 _color;

    public SpaceshipLight (Color32 color)
    {
        _color = color;
    }

    public Color32 GetColor()
    {
        return _color;
    }
    
    public static SpaceshipLight WHITE = new SpaceshipLight(new Color32(255, 255, 255, 255));
    public static SpaceshipLight YELLOW = new SpaceshipLight(new Color32(255, 244, 0, 255));
    public static SpaceshipLight RED = new SpaceshipLight(new Color32(255, 0, 0, 255));
}