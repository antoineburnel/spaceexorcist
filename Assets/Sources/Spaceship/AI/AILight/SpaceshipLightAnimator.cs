using UnityEngine;
using System.Collections;
using Utils;
using UnityEngine.Rendering.Universal;

public class SpaceshipLightAnimator : MonoBehaviour
{
    private Light2D _light;

    private void Awake()
    {
        _light = GetComponent<Light2D>();
    }

    public void SetLight(Color newColor)
    {
        _light.color = newColor;
    }
}