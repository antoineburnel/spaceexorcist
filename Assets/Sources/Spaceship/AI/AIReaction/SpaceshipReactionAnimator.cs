using UnityEngine;
using System.Collections;
using Utils;

public class SpaceshipReactionAnimator : MonoBehaviour
{
    private Transform _transform;
    private SpriteRenderer _sprite;

    private void Awake()
    {
        _transform = transform;
        _sprite = GetComponent<SpriteRenderer>();
    }

    public void SetReaction(Sprite newSprite)
    {
        _sprite.sprite = newSprite;
    }
}