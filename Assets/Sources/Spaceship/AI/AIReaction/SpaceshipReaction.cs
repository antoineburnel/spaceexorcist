using UnityEngine;

public class SpaceshipReaction : MonoBehaviour
{
    public static SpaceshipReactionData NO = new SpaceshipReactionData(null, 0.4f, 0.25f, 0.25f, 1.25f);
    public static SpaceshipReactionData QUESTION;
    public static SpaceshipReactionData EXCLAMATION;

    private void Awake()
    {
        QUESTION = new SpaceshipReactionData(LoadSprite("AI/Searching/question"), 1.2f, 0.25f, 0.125f, 1.4f);
        EXCLAMATION = new SpaceshipReactionData(LoadSprite("AI/Chasing/exclamation"), 1.2f, 0.25f, 0.125f, 1.4f);
    }

    private static Sprite LoadSprite(string resourcePath)
    {
        return Resources.Load<Sprite>(resourcePath);
    }
}