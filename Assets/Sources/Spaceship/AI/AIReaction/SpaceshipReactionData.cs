using UnityEngine;

public class SpaceshipReactionData
{

    private Sprite _sprite;
    private float _finalPosY;
    private float _duration;
    private float _timeCodeSpriteChangement;
    private float _timeCodePosY;

    public SpaceshipReactionData(Sprite sprite, float finalPosY, float duration, float timeCodeSpriteChangement, float timeCodePosY)
    {
        _sprite = sprite;
        _finalPosY = finalPosY;
        _duration = duration;
        _timeCodeSpriteChangement = timeCodeSpriteChangement;
        _timeCodePosY = timeCodePosY;
    }

    public Sprite GetSprite()
    {
        return _sprite;
    }

    public float GetFinalPosY()
    {
        return _finalPosY;
    }

    public float GetDuration()
    {
        return _duration;
    }

    public float GetTimeCodeSpriteChangement()
    {
        return _timeCodeSpriteChangement;
    }

    public float GetTimeCodePosY()
    {
        return _timeCodePosY;
    }
}