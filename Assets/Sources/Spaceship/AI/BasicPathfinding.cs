using UnityEngine;
using Utils;
public static class BasicPathfinding
{
    private static int enemyPathfindingMask = 1 << (int)LayerMeaning.WALL;
    
    public static bool CanGoFromPointAToPointB(Vector3 positionA, Vector3 positionB)
    {
        RaycastHit2D hit = Physics2D.Linecast(positionA, positionB, enemyPathfindingMask);
        return hit.collider == null;
    }
}