using UnityEngine;
using Ship.Weapon;
using System;
using TMPro;
using UnityEngine.UI;
using Utils;
using System.Collections;

public class AmmoBarUISystem : MonoBehaviour, IComponentUI
{
    private Animator _animator;

    private static float TIME_TO_HIDE = 0.5f;
    private static float TIME_TO_SHOW = 0.5f;
    private static float SHOOT_SCALE_INCREASE_PERC = 0.2f;
    private static float RELOAD_SCALE_DECREASE_PERC = 0.4f;
    private static Vector3 OFFSET_ANIM = new Vector3(0, -100, 0);
    private Vector3 _activatedPosition;
    private Vector3 _desactivatedPosition;

    private Coroutine _componentAnimationCoroutine;
    private Transform _transform;
    private TMP_Text _text;
    private Image _img;
    private static Color _ACTIVATED_IMG = new Color(1f, 1f, 1f, 0.8f);
    private static Color _DEACTIVATED_IMG = new Color(1f, 1f, 1f, 0f);
    private Color _activatedColor = new Color(1f, 1f, 1f, 1f);
    private Color _desactivatedColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    private Transform _textTransform;
    private Vector3 _startScale;

    public void Init()
    {
        _transform = transform;
        _animator = _transform.GetComponent<Animator>();
        _textTransform = _transform.GetChild(0);
        _text = _textTransform.GetComponent<TMP_Text>();
        _img = _transform.GetChild(1).GetComponent<Image>();

        _startScale = _textTransform.localScale;
        _activatedPosition = _transform.position;
        _desactivatedPosition = _transform.position + OFFSET_ANIM;
        _transform.position = _desactivatedPosition;

        _componentAnimationCoroutine = null;
        Show();
    }

    public void ActivateUI()
    {
        _transform.gameObject.SetActive(true);
    }

    public void DesactivateUI()
    {
        _transform.gameObject.SetActive(false);
    }

    public void Hide()
    {
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _activatedPosition, _desactivatedPosition, TIME_TO_HIDE, null));
    }

    public void Show()
    {
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _desactivatedPosition, _activatedPosition, TIME_TO_SHOW, null));
    }

    public void SetAmmo(int currentAmmo, int magSize)
    {
        _text.SetText(currentAmmo + " / " + magSize);
    }

    public void OnFire(WeaponData data)
    {
        SetAmmo(data.GetCurrentBulletInMag(), data.GetMagSize());
        StartCoroutine(ShootingAnimation(data.GetTimeBetweenBullets()));
    }

    public void OnStartReload(WeaponData data, float duration)
    {
        _text.color = _desactivatedColor;
        StartCoroutine(FillReloadingCircleDuringSeconds(duration));
        StartCoroutine(ReloadAnimation(duration));
    }

    public void OnReload(WeaponData data)
    {
        _text.color = _activatedColor;
        SetAmmo(data.GetCurrentBulletInMag(), data.GetMagSize());
    }

    public IEnumerator FillReloadingCircleDuringSeconds(float duration)
    {
        float elapsed = 0;
        float t = 0;
        _img.fillAmount = 0;
        while (elapsed < duration)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / duration;
            _img.fillAmount = t;
            yield return null;
        }
        _img.fillAmount = 1f;
    }

    public IEnumerator ShootingAnimation(float duration)
    {
        float elapsed = 0;
        float t = 0;
        Vector3 startScale = _startScale;
        Vector3 finalScale = _startScale * SHOOT_SCALE_INCREASE_PERC;

        while (elapsed < duration)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / duration;
            _textTransform.localScale = Vector3.Lerp(startScale, finalScale, SmoothFunction.EaseInOutSquare(SmoothFunction.TimeToTriangle(t)));
            yield return null;
        }
        _textTransform.localScale = startScale;
    }

    public IEnumerator ReloadAnimation(float duration)
    {
        float elapsed = 0;
        float t = 0;
        Vector3 startScale = _startScale;
        Vector3 finalScale = _startScale - (startScale * RELOAD_SCALE_DECREASE_PERC);

        while (elapsed < duration)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / duration;
            _img.color = Color.Lerp(_DEACTIVATED_IMG, _ACTIVATED_IMG, SmoothFunction.EaseInOutSquare(SmoothFunction.TimeToFlatTriangle(t, 3f)));
            _textTransform.localScale = Vector3.Lerp(startScale, finalScale, SmoothFunction.EaseInOutSquare(SmoothFunction.TimeToFlatTriangle(t, 3f)));
            yield return null;
        }
        _textTransform.localScale = startScale;
        _img.color = _DEACTIVATED_IMG;
    }
}