using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections;
using Utils;
using Ship.Weapon;
using Ship;

public class WeaponUISystem : MonoBehaviour, IComponentUI
{
    private const int NO_WEAPON_INDEX = -1;

    //ANIMATION DURATION
    private const float TIME_TO_HIDE = 0.4f;
    private const float TIME_TO_SHOW = 0.4f;

    //KEY SPRITE
    private static Color32 VISIBLE_KEY = new Color32(255, 255, 255, 255);
    private static Color32 HIDDEN_KEY = new Color32(255, 255, 255, 80);

    //BACKGROUND SPRITE
    private static Color32 WHITE_ENABLED_BG = new Color32(255, 255, 255, 100);
    private static Color32 RED_ENABLED_BG = new Color32(255, 172, 177, 100);
    private static Color32 WHITE_DISABLED_BG = new Color32(255, 255, 255, 40);
    private static Color32 RED_DISABLED_BG = new Color32(255, 172, 177, 40);
    private const float THRESHOLD_CRITICAL_BG = 1 / 3f;
    private const float MAXIMUM_AMMO = 1f;

    //BORDER SPRITE
    private static Color32 VISIBLE_BD = new Color32(255, 255, 255, 40);
    private static Color32 HIDDEN_BD = new Color32(255, 255, 255, 20);

    //WEAPON SPRITE
    private static Color32 VISIBLE_SPRITE = new Color32(255, 255, 255, 255);
    private static Color32 HIDDEN_SPRITE = new Color32(255, 255, 255, 120);

    //HIDE AND SHOW ANIMATIONS VARIABLES
    private static Vector3 OFFSET_ANIM = new Vector3(0, -100, 0);
    private Vector3 _activatedPosition;
    private Vector3 _desactivatedPosition;

    private Coroutine _animCoroutine;
    private Transform _transform;

    private Image[] _sprites;
    private Image[] _keys;
    private Image[] _borders;
    private Image[] _backgrounds;

    private int _highlightedWeaponIndex;

    public void Init ()
    {

        _transform = transform;

        _sprites = new Image[2];
        _keys = new Image[2];
        _borders = new Image[2];
        _backgrounds = new Image[2];

        _backgrounds[0] = _transform.GetChild(0).GetChild(0).GetComponent<Image>();
        _borders[0] = _transform.GetChild(0).GetChild(1).GetComponent<Image>();
        _sprites[0] = _transform.GetChild(0).GetChild(2).GetComponent<Image>();
        _keys[0] = _transform.GetChild(0).GetChild(3).GetComponent<Image>();

        _backgrounds[1] = _transform.GetChild(1).GetChild(0).GetComponent<Image>();
        _borders[1] = _transform.GetChild(1).GetChild(1).GetComponent<Image>();
        _sprites[1] = _transform.GetChild(1).GetChild(2).GetComponent<Image>();
        _keys[1] = _transform.GetChild(1).GetChild(3).GetComponent<Image>();

        _activatedPosition = _transform.position;
        _desactivatedPosition = _transform.position + OFFSET_ANIM;
        _transform.position = _desactivatedPosition;

        _animCoroutine = null;

        Show();
        _highlightedWeaponIndex = 1;
    }

    public void ActivateUI()
    {
        _transform.gameObject.SetActive(true);
    }

    public void DesactivateUI()
    {
        _transform.gameObject.SetActive(false);
    }

    public void Hide()
    {
        Action pointer = DesactivateUI;
        ActivateUI();
        _animCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _activatedPosition, _desactivatedPosition, TIME_TO_HIDE, pointer));
    }

    public void Show()
    {
        ActivateUI();
        _animCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _desactivatedPosition, _activatedPosition, TIME_TO_SHOW, null));
    }

    public void SetWeapon1()
    {
        SetWeapon(0);
    }

    public void SetWeapon2()
    {
        SetWeapon(1);
    }

    private void SetWeapon(int index)
    {
        if (_highlightedWeaponIndex == index) return;
        _highlightedWeaponIndex = index;

        StartCoroutine(ShowWeaponAnimation(index));
    }

    private IEnumerator ShowWeaponAnimation(int index)
    {
        float elapsed = 0f;
        float t = 0f;

        Color startBG;
        Color endBG;

        if (_backgrounds[index].fillAmount <= THRESHOLD_CRITICAL_BG)
        {
            startBG = RED_DISABLED_BG;
            endBG = RED_ENABLED_BG;
        }
        else
        {
            startBG = WHITE_DISABLED_BG;
            endBG = WHITE_ENABLED_BG;
        }

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_SHOW;

            _keys[index].color = Color.Lerp(HIDDEN_KEY, VISIBLE_KEY, SmoothFunction.EaseInOutSquare(t));
            _backgrounds[index].color = Color.Lerp(startBG, endBG, SmoothFunction.EaseInOutSquare(t));
            _sprites[index].color = Color.Lerp(HIDDEN_SPRITE, VISIBLE_SPRITE, SmoothFunction.EaseInOutSquare(t));
            _borders[index].color = Color.Lerp(HIDDEN_BD, VISIBLE_BD, SmoothFunction.EaseInOutSquare(t));

            yield return null;
        }

        _keys[index].color = VISIBLE_KEY;
        _backgrounds[index].color = endBG;
        _sprites[index].color = VISIBLE_SPRITE;
        _borders[index].color = VISIBLE_BD;
    }

    public void UnsetWeapon()
    {
        if (_highlightedWeaponIndex == -1) return;

        StartCoroutine(HideWeaponAnimation(_highlightedWeaponIndex));
        _highlightedWeaponIndex = -1;
    }

    private IEnumerator HideWeaponAnimation(int index)
    {
        float elapsed = 0f;
        float t = 0f;

        Color startBG;
        Color endBG;

        if (_backgrounds[index].fillAmount <= THRESHOLD_CRITICAL_BG)
        {
            startBG = RED_ENABLED_BG;
            endBG = RED_DISABLED_BG;
        }
        else
        {
            startBG = WHITE_ENABLED_BG;
            endBG = WHITE_DISABLED_BG;
        }

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_SHOW;

            _keys[index].color = Color.Lerp(VISIBLE_KEY, HIDDEN_KEY, SmoothFunction.EaseInOutSquare(t));
            _backgrounds[index].color = Color.Lerp(startBG, endBG, SmoothFunction.EaseInOutSquare(t));
            _sprites[index].color = Color.Lerp(VISIBLE_SPRITE, HIDDEN_SPRITE, SmoothFunction.EaseInOutSquare(t));
            _borders[index].color = Color.Lerp(VISIBLE_BD, HIDDEN_BD, SmoothFunction.EaseInOutSquare(t));

            yield return null;
        }

        _keys[index].color = HIDDEN_KEY;
        _backgrounds[index].color = endBG;
        _sprites[index].color = HIDDEN_SPRITE;
        _borders[index].color = HIDDEN_BD;
    }

    public void ChangedShip(Spaceship newship)
    {
        UpdateWeaponSprites(newship);
        UpdateAmmoBar(newship.GetWeaponData(0), 0);
        UpdateAmmoBar(newship.GetWeaponData(1), 1);
    }

    public void OnFire(WeaponData data)
    {
        UpdateAmmoBar(data, _highlightedWeaponIndex);
    }

    public void OnReload(WeaponData data)
    {
        UpdateAmmoBar(data, _highlightedWeaponIndex);
    }

    private void UpdateWeaponSprites(Spaceship newship)
    {
        _sprites[0].sprite = newship.GetWeaponSprite(0);
        _sprites[1].sprite = newship.GetWeaponSprite(1);
    }

    private void UpdateAmmoBar(WeaponData data, int index)
    {
        if (index == NO_WEAPON_INDEX)
        {
            return;
        }
        float percentage = (float)(data.GetCurrentBulletInMag()) / (float)(data.GetMagSize());
        _backgrounds[index].fillAmount = percentage;
        if (percentage == MAXIMUM_AMMO)
        {
            _backgrounds[index].fillAmount = MAXIMUM_AMMO;
            _backgrounds[index].color = WHITE_ENABLED_BG;
        }
        else if (percentage <= THRESHOLD_CRITICAL_BG)
        {
            _backgrounds[index].color = RED_ENABLED_BG;
        }
    }
}