using UnityEngine;
using UnityEngine.UI;
using Ship;
using System;
using TMPro;
using Utils;

public class TimerTextUISystem : MonoBehaviour, IComponentUI
{
    public static float TIME_TO_HIDE = 0.5f;
    public static float TIME_TO_SHOW = 0.5f;
    public static Vector3 OFFSET_ANIM = new Vector3(0, 100, 0);

    private Transform _transform;
    private TMP_Text _text;

    private Vector3 _activatedPosition;
    private Vector3 _desactivatedPosition;

    private Coroutine _componentAnimationCoroutine;
    private bool _increment;

    private int _minutes;
    private int _seconds;
    private int _milliseconds;

    private float _referenceTime;

    public void Init ()
    {
        _transform = transform;
        _increment = false;

        _text = _transform.GetChild(0).GetComponent<TMP_Text>();

        _activatedPosition = _transform.position;
        _desactivatedPosition = _transform.position + OFFSET_ANIM;
        _transform.position = _desactivatedPosition;

        _componentAnimationCoroutine = null;

        UpdateText();
        Show();
    }

    public void ActivateUI()
    {
        _transform.gameObject.SetActive(true);
    }

    public void DesactivateUI()
    {
        _transform.gameObject.SetActive(false);
    }

    public void Hide()
    {
        Action pointer = DesactivateUI;
        ActivateUI();
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _activatedPosition, _desactivatedPosition, TIME_TO_HIDE, pointer));
    }

    public void Show()
    {
        ActivateUI();
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _desactivatedPosition, _activatedPosition, TIME_TO_SHOW, null));
    }

    public void StartClock()
    {
        _increment = true;
        _referenceTime = Time.timeSinceLevelLoad;
    }

    public void StopClock()
    {
        _increment = false;
    }

    public void UpdateText()
    {
        if (!_increment) return;
        float _relativeTime = Time.timeSinceLevelLoad - _referenceTime; 
        _minutes = (int)(_relativeTime / 60f) % 60;
        _seconds = (int)(_relativeTime) % 60;
        _milliseconds = (int)(_relativeTime * 1000f) % 1000;
        _text.SetText(string.Format("{0} : {1:00} : {2:000}", _minutes, _seconds, _milliseconds));
    }

    public int GetMinutes()
    {
        return _minutes;
    }

    public int GetSeconds()
    {
        return _seconds;
    }

    public int GetMilliseconds()
    {
        return _milliseconds;
    }

    public void OnDeath(Transform transform, ControlledByPlayer controlled)
    {
        _increment = false;
    }
}