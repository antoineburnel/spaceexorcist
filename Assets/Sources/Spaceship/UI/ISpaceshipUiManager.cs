using Ship;
using Ship.Weapon;
public interface ISpaceshipUiManager : IComponentUI
{
    public void Inject (Spaceship spaceship, SpaceshipData shipData, WeaponData weaponData);
}
