using UnityEngine;
using System.Collections.Generic;
using Ship;
using Ship.Weapon;
public class PlayerSpaceshipUiManager : MonoBehaviour, ISpaceshipUiManager
{

    private static PlayerSpaceshipUiManager _instance;

    private List<IComponentUI> _uiComponents;
    private Transform _transform;

    private HealthBarUISystem _healthBar;
    private AmmoBarUISystem _ammoBar;
    private KeyEUISystem _actionE;
    private KeyAUISystem _actionA;
    private WeaponUISystem _weaponsUi;
    private TimerTextUISystem _timerUI;

    private ControlledByPlayer _controlledByPlayer;

    private void Awake()
    {
        _instance = this;
        Init();
    }

    public static PlayerSpaceshipUiManager GetInstance()
    {
        return _instance;
    }

    public void Init()
    {
        Transform _canvas = transform;
        _transform = transform;
        _controlledByPlayer = _transform.GetComponent<ControlledByPlayer>();
        _uiComponents = new List<IComponentUI>();

        _healthBar = _transform.GetChild(0).GetComponent<HealthBarUISystem>();
        _uiComponents.Add(_healthBar);

        _ammoBar = _transform.GetChild(1).GetComponent<AmmoBarUISystem>();
        _uiComponents.Add(_ammoBar);

        _actionE = _transform.GetChild(2).GetComponent<KeyEUISystem>();
        _uiComponents.Add(_actionE);

        _actionA = _transform.GetChild(3).GetComponent<KeyAUISystem>();
        _uiComponents.Add(_actionA);

        _weaponsUi = _transform.GetChild(4).GetComponent<WeaponUISystem>();
        _uiComponents.Add(_weaponsUi);

        _timerUI = _transform.GetChild(5).GetComponent<TimerTextUISystem>();
        _uiComponents.Add(_timerUI);

        foreach (IComponentUI component in _uiComponents)
        {
            component.Init();
        }

        Arena.Instance.Inject(_timerUI);
    }

    public void Inject(Spaceship spaceship, SpaceshipData shipData, WeaponData weaponData)
    {
        OnDamage(shipData, _controlledByPlayer);
        OnFire(weaponData);
        ChangedShip(spaceship);
        UnsetWeapon();
        SetWeapon1();
    }

    public void ActivateUI()
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.ActivateUI();
        }
    }

    public void DesactivateUI()
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.DesactivateUI();
        }
    }

    public void Hide()
    {
        _healthBar.Hide();
        _ammoBar.Hide();
        _weaponsUi.Hide();
    }

    public void Show()
    {
        _healthBar.Show();
        _ammoBar.Show();
        _weaponsUi.Show();
    }

    public void OnDamage(SpaceshipData data, ControlledByPlayer controlled)
    {
        if (controlled == null || !controlled.enabled) return;

        foreach (IComponentUI component in _uiComponents)
        {
            component.OnDamage(data, controlled);
        }
    }

    public void OnDeath(Transform transform, ControlledByPlayer controlled)
    {
        if (controlled == null || !controlled.enabled) return;

        foreach (IComponentUI component in _uiComponents)
        {
            component.OnDeath(transform, controlled);
        }

        Hide();
    }

    public void OnFire(WeaponData data)
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.OnFire(data);
        }
    }

    public void OnReload(WeaponData data)
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.OnReload(data);
        }
    }

    public void OnStartReload(WeaponData data, float duration)
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.OnStartReload(data, duration);
        }
    }

    public void SetMoveE(string action)
    {
        _actionE.SetMoveE(action);
    }

    public void UnsetMoveE()
    {
        _actionE.UnsetMoveE();
    }

    public void SetMoveA(string action)
    {
        _actionA.SetMoveA(action);
    }

    public void UnsetMoveA()
    {
        _actionA.UnsetMoveA();
    }

    public void UnsetWeapon()
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.UnsetWeapon();
        }
    }

    public void SetWeapon1()
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.SetWeapon1();
        }
    }

    public void SetWeapon2()
    {
        foreach (IComponentUI component in _uiComponents)
        {
            component.SetWeapon2();
        }
    }

    public void ChangingWeapon()
    {
        _ammoBar.Hide();
    }

    public void HasChangedWeapon(WeaponData weaponData)
    {
        _ammoBar.OnFire(weaponData);
        _ammoBar.Show();
    }

    public void ChangingShip(Spaceship newship, float duration)
    {
        Hide();
        _actionE.Hide();
        _actionA.Hide();
        foreach (IComponentUI component in _uiComponents)
        {
            component.ChangingShip(newship, duration);
        }
    }

    public void ChangedShip(Spaceship newship)
    {
        Show();
        _controlledByPlayer = newship.GetComponent<ControlledByPlayer>();
        foreach (IComponentUI component in _uiComponents)
        {
            component.ChangedShip(newship);
        }
    }
}