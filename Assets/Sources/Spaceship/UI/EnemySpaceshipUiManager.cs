using Ship;
using Ship.Weapon;
public class EnemySpaceshipUiManager : ISpaceshipUiManager, ISpaceshipEvents
{
    public void Init()
    {

    }

    public void Inject (Spaceship spaceship, SpaceshipData shipData, WeaponData weaponData)
    {

    }

    public void ActivateUI()
    {

    }

    public void DesactivateUI()
    {

    }

    public void Hide()
    {

    }

    public void Show()
    {

    }
}