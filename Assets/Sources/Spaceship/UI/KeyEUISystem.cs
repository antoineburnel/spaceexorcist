using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;
using Utils;

public class KeyEUISystem : MonoBehaviour, IComponentUI
{
    private static float TIME_TO_HIDE = 0.1f;
    private static float TIME_TO_SHOW = 0.1f;
    private static Vector3 OFFSET_ANIM = new Vector3(-500, 0, 0);
    private Vector3 _activatedPosition;
    private Vector3 _desactivatedPosition;

    private Coroutine _componentAnimationCoroutine;
    private Transform _transform;
    private TMP_Text _text;
    private Image _img;
    private bool _isActivated;
    private string _actionText;

    public void Init()
    {
        _transform = transform;

        _text = _transform.GetChild(0).GetComponent<TMP_Text>();
        _img = _transform.GetChild(1).GetComponent<Image>();

        _activatedPosition = _transform.position;
        _desactivatedPosition = _transform.position + OFFSET_ANIM;
        _transform.position = _desactivatedPosition;

        _componentAnimationCoroutine = null;
        _isActivated = false;
    }

    public void ActivateUI()
    {
        _isActivated = true;
        _transform.gameObject.SetActive(true);
    }

    public void DesactivateUI()
    {
        _isActivated = false;
        _transform.gameObject.SetActive(false);
    }

    public void Hide()
    {
        Action pointer = DesactivateUI;
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _activatedPosition, _desactivatedPosition, TIME_TO_HIDE, pointer));
    }

    public void Show()
    {
        ActivateUI();
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _desactivatedPosition, _activatedPosition, TIME_TO_SHOW, null));
    }

    public void SetMoveE(string action)
    {
        if (_isActivated)
        {
            _text.SetText(action);
            return;
        }
        Show();
        _text.SetText(action);
    }

    public void UnsetMoveE()
    {
        if (_isActivated)
        {
            Hide();
            return;
        }
    }

    private void UpdateText()
    {
        _text.SetText(_actionText);
    }
}