using UnityEngine;
using UnityEngine.UI;
using Ship;
using System;
using System.Collections;
using Utils;

public class HealthBarUISystem : MonoBehaviour, IComponentUI
{
    public static float TIME_TO_HIDE = 0.5f;
    public static float TIME_TO_SHOW = 0.5f;
    public static float GHOST_BAR_TIME_TO_SYNC = 0.2f;
    public static Vector3 OFFSET_ANIM = new Vector3(0, -100, 0);

    private Transform _transform;
    private Image _border;
    private Image _fillingPart;
    private Image _ghostFillingPart;
    private bool _isInDanger;

    private Color _normalBarColor;
    private Color _dangerBarColor;
    private Color _normalGhostBarColor;
    private Color _dangerGhostBarColor;
    private static float _dangerThreshold = 1f / 3f;

    private Vector3 _activatedPosition;
    private Vector3 _desactivatedPosition;

    private Coroutine _ghostBarCoroutine;
    private Coroutine _componentAnimationCoroutine;

    public void Init()
    {
        _transform = transform;
        _isInDanger = false;

        _normalBarColor = new Color32(255, 255, 255, 200);
        _dangerBarColor = new Color32(255, 138, 138, 200);
        _normalGhostBarColor = new Color32(255, 255, 255, 90);
        _dangerGhostBarColor = new Color32(255, 138, 138, 90);

        _ghostFillingPart = _transform.GetChild(0).GetComponent<Image>();
        _fillingPart = _transform.GetChild(1).GetComponent<Image>();
        _border = _transform.GetChild(2).GetComponent<Image>();

        _activatedPosition = _transform.position;
        _desactivatedPosition = _transform.position + OFFSET_ANIM;
        _transform.position = _desactivatedPosition;

        _ghostBarCoroutine = null;
        _componentAnimationCoroutine = null;

        SetHealth(1f);
        Show();
    }

    public void ActivateUI()
    {
        _transform.gameObject.SetActive(true);
    }

    public void DesactivateUI()
    {
        _transform.gameObject.SetActive(false);
    }

    public void Hide()
    {
        Action pointer = DesactivateUI;
        ActivateUI();
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _activatedPosition, _desactivatedPosition, TIME_TO_HIDE, pointer));
    }

    public void Show()
    {
        ActivateUI();
        _componentAnimationCoroutine = StartCoroutine(BasicAnim.MoveObjFromPosToPos(_transform, _desactivatedPosition, _activatedPosition, TIME_TO_SHOW, null));
    }

    public void SetHealth(float ratio)
    {
        UpdateDangerVariable(ratio);
        UpdateGhostBar(ratio);
        UpdateBarLength(ratio);
        UpdateBarColor();
    }

    private void UpdateBarLength(float ratio)
    {
        _fillingPart.fillAmount = ratio;
    }

    private void UpdateGhostBar(float newRatio)
    {
        if (_ghostBarCoroutine != null)
        {
            StopCoroutine(_ghostBarCoroutine);
        }
        float oldRatio = _ghostFillingPart.fillAmount;
        _ghostBarCoroutine = StartCoroutine(SetHealthGhostBarToFillAmount(_ghostFillingPart, oldRatio, newRatio, GHOST_BAR_TIME_TO_SYNC));
    }

    private void UpdateBarColor()
    {
        if (_isInDanger)
        {
            _fillingPart.color = _dangerBarColor;
            _ghostFillingPart.color = _dangerGhostBarColor;
        }
        else
        {
            _fillingPart.color = _normalBarColor;
            _ghostFillingPart.color = _normalGhostBarColor;
        }
    }

    private void UpdateDangerVariable(float ratio)
    {
        if (ratio < _dangerThreshold)
        {
            _isInDanger = true;
        }
        else
        {
            _isInDanger = false;
        }
    }

    public void OnDamage(SpaceshipData data, ControlledByPlayer controlled)
    {
        if (!controlled.enabled) return;
        SetHealth(data.GetHealth() / data.GetMaxHealth());
    }

    public void OnDeath(Transform transform, ControlledByPlayer controlled)
    {
        SetHealth(0f);
    }

    public IEnumerator SetHealthGhostBarToFillAmount ( Image ghostBar, float startAmount, float endAmount, float duration )
    {
        float elapsed = 0;
        while ( elapsed < duration )
        {
            elapsed += Time.unscaledDeltaTime;
            ghostBar.fillAmount = Mathf.Lerp( startAmount, endAmount, elapsed / duration );
            yield return null;
        }
        ghostBar.fillAmount = endAmount;
    }
}