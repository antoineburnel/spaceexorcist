using Ship;
using Ship.Weapon;
using UnityEngine;
public interface ISpaceshipEvents
{
    public virtual void OnFire(WeaponData data) { }
    public virtual void OnStartReload(WeaponData data, float duration) { }
    public virtual void OnReload(WeaponData data) { }
    public virtual void OnDamage(SpaceshipData data, ControlledByPlayer controlled) { }
    public virtual void OnHeal(SpaceshipData data) { }
    public virtual void OnDeath(Transform transform, ControlledByPlayer controlled) { }
    public virtual void SetMoveA(string action) { }
    public virtual void UnsetMoveA() { }
    public virtual void SetMoveE(string action) { }
    public virtual void UnsetMoveE() { }
    public virtual void UnsetWeapon() { }
    public virtual void SetWeapon1() { }
    public virtual void SetWeapon2() { }
    public virtual void ChangingWeapon() { }
    public virtual void HasChangedWeapon(WeaponData weaponData) { }
    public virtual void ChangingShip(Spaceship newship, float duration) { }
    public virtual void ChangedShip(Spaceship newship) { }
}