using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SpaceshipUICoroutineManager : MonoBehaviour
{
    public IEnumerator SetHealthGhostBarToFillAmount ( Image ghostBar, float startAmount, float endAmount, float duration )
    {
        float elapsed = 0;
        while ( elapsed < duration )
        {
            elapsed += Time.deltaTime;
            ghostBar.fillAmount = Mathf.Lerp( startAmount, endAmount, elapsed / duration );
            yield return null;
        }
        ghostBar.fillAmount = endAmount;
    }

    public IEnumerator MoveObjFromPosToPos ( Transform obj, Vector3 origin, Vector3 arrival, float duration, Action methodToCallAtTheEnd )
    {
        float elapsed = 0;
        while ( elapsed < duration )
        {
            elapsed += Time.deltaTime;
            obj.position = Vector3.Lerp( origin, arrival, elapsed / duration );
            yield return null;
        }
        if (methodToCallAtTheEnd != null) methodToCallAtTheEnd();
    }

    public IEnumerator FillReloadingCircleDuringSeconds ( Image img, float duration )
    {
        float elapsed = 0;
        img.fillAmount = 0;
        duration -= 0.1f;
        yield return new WaitForSeconds( 0.1f );
        while ( elapsed < duration )
        {
            elapsed += Time.deltaTime;
            img.fillAmount = elapsed / duration;
            yield return null;
        }
        img.fillAmount = 1f;
    }
}
