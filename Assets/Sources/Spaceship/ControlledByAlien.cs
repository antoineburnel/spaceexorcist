using Ship;
using UnityEngine;

public class ControlledByAlien : MonoBehaviour
{

    private AlienSpaceship _ship;
    [SerializeField] private bool _enabled = true;

    private void Awake()
    {
        _ship = gameObject.GetComponent<AlienSpaceship>();
    }

    private void Start()
    {
        _ship.Init();
        if (!_enabled) this.enabled = false;
    }

    private void Update()
    {
        _ship.DoUpdate();
    }

    private void FixedUpdate()
    {
        _ship.DoFixedUpdate();
    }
}