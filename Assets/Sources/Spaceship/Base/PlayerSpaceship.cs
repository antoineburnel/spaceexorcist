namespace Ship
{
    using UnityEngine;
    using Ship.Movement;
    using Ship.Weapon;
    using Ship.Camera;
    using Utils;
    using Ship.Animation;

    [RequireComponent(typeof(ControlledByPlayer))]
    public abstract class PlayerSpaceship : Spaceship, ICanFeelDanger, ICanBeDetect
    {
        private SpaceshipCamera _spaceshipCamera;
        [SerializeField] private bool _isInitiallyLanded = false;
        private ControlledByPlayer _controlled;
        
        public void Init()
        {
            _controlled = GetComponent<ControlledByPlayer>();
            InitUi();
            InitBasicComponents();
            if (_isInitiallyLanded)
            {
                _shipState = new SpaceshipStateLanded();
            }
            else
            {
                _shipState = new SpaceshipStateFlying();
            }
            InitMovementController();
            InitWeapon();
            InitCamera();
        }

        public override void InitializeAnimator()
        {
            _animator = _transform.GetComponent<PlayerSpaceshipAnimator>();
            _animator.Init(_shipData);
        }

        private void InitUi()
        {
            _uiManager = PlayerSpaceshipUiManager.GetInstance();
        }

        public void UpdateUI()
        {
            _uiManager.Inject(this, _shipData, _weaponController.GetData());

            if (_actionA != null) _uiManager.SetMoveA(_actionA.GetName(this));
            else _uiManager.UnsetMoveA();

            if (_actionE != null) _uiManager.SetMoveE(_actionE.GetName(this));
            else _uiManager.UnsetMoveE();
        }

        private void InitMovementController()
        {
            _movementController = GetComponent<PlayerSpaceshipController>();
            PlayerSpaceshipController tmp = (PlayerSpaceshipController)_movementController;
            tmp.Init(_rb, _shipData, _transform);
            _animator.DoUpdate(_shipData, _movementController.GetSpaceshipMovementData());
        }

        public override void InitializeSpaceshipData()
        {
            _shipData = new PlayerSpaceshipData(_datas.BaseShipData, _uiManager, _visualEffects, _transform);
        }

        private void InitCamera()
        {
            _spaceshipCamera = new SpaceshipCamera(_transform, _rb, _datas.CameraSettings);
        }

        private void InitWeapon()
        {
            IWeaponControllerStrategy weaponStrategy = new PlayerWeaponControllerStrategy();

            BasicWeaponData weapon1Data;
            if (_datas.BaseWeapon1Data != null) weapon1Data = new BasicWeaponData(_datas.BaseWeapon1Data);
            else weapon1Data = null;

            BasicWeaponData weapon2Data;
            if (_datas.BaseWeapon2Data != null) weapon2Data = new BasicWeaponData(_datas.BaseWeapon2Data);
            else weapon2Data = null;

            _weaponController = new WeaponController(this, _transform, weapon1Data, weapon2Data, weaponStrategy);
            weaponStrategy.Init(_weaponController);
        }

        public void DoUpdate()
        {
            _animator.DoUpdate(_shipData, _movementController.GetSpaceshipMovementData());

            if (_shipState.CanFire())
            {
                _weaponController.UpdateStrategy();
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                TriggerA();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                TriggerE();
            }
        }

        public void DoFixedUpdate()
        {
            if (_shipState.CanMove())
            {
                _movementController.DoUpdate();
            }
        }

        public void DoLateUpdate()
        {
            _spaceshipCamera.Update(this, _shipData.GetSpeed());
        }

        public Vector3 GetNextCameraPosition()
        {
            return _spaceshipCamera.GetNextPosition(this, _shipData.GetSpeed(), _datas.CameraSettings);
        }

        public float GetNextFOV()
        {
            return _spaceshipCamera.GetNextFOV(_shipData.GetSpeed(), _datas.CameraSettings);
        }

        public override void InPlanetGravity(float distance, float maxDistance)
        {
            float ratioInIt = 1f - (distance / maxDistance);
            _visualEffects.SetGravitySensation(ratioInIt);
        }

        public override void InBlackHoleGravity(float distance, float maxDistance)
        {
            float ratioInIt = distance / maxDistance;
            _visualEffects.InBlackHole(ratioInIt);
        }

        public void InDanger(Transform source, float maxDistance)
        {
            float distance = Heuristics.euclideanDistance(source.position, _transform.position);
            float ratioInIt = 1f - (distance / maxDistance);
            _visualEffects.SetDangerSensation(ratioInIt);
        }

        public void ResetDanger()
        {
            _visualEffects.SetDangerSensation(0f);
        }

        public override void InitializeVisualEffects()
        {
            _visualEffects = new PlayerSpaceshipVisualEffects(_transform);
        }

        public override void ChangingShip(Spaceship _newship, float duration)
        {
            _uiManager.ChangingShip(_newship, duration);
            _spaceshipCamera.ChangingShip(_newship, duration);
            _isChangingShip = true;
        }

        public override void ChangedShip(Spaceship _newship)
        {
            _uiManager.ChangedShip(_newship);
            _isChangingShip = false;
        }

        public override bool IsHuman()
        {
            return true;
        }

        public override bool IsAlien()
        {
            return false;
        }

        public override bool IsRobot()
        {
            return false;
        }

        public override bool IsParasitable()
        {
            return true;
        }

        public override bool IsHackable()
        {
            return false;
        }

        public override EnemySpaceshipAI GetAI()
        {
            return null;
        }
    
        public override void Hit(float damage, Transform enemyTransform, bool ignoreDamageProtection)
        {
            if (!_canTakeDamage && !ignoreDamageProtection)
            {
                return;
            }
            _shipData.Damage(damage, enemyTransform.position);
            if (_shipData.IsDead())
            {
                Destroy(gameObject);
            }
        }

        public override void OnOnDestroy()
        {
            Arena.Instance.OnPlayerDeath();
        }
    }
}