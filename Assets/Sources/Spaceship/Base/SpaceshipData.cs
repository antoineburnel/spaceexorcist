namespace Ship
{

    using UnityEngine;

    public abstract class SpaceshipData
    {
        protected string _name;
        protected float _health;
        protected float _maxSpeed;
        protected float _speed;
        protected float _acceleration;
        protected float _deceleration;
        protected float _turningSpeed;
        protected float _maxHealth;
        protected Transform _transform;
        protected ISpaceshipUiManager _uiManager;
        protected Color _minColorBoost;
        protected Color _maxColorBoost;
        protected SpaceshipVisualEffects _visualEffects;
        protected ControlledByPlayer _controlledByPlayer;

        public SpaceshipData(SpaceshipBaseData baseData, ISpaceshipUiManager uiManager, SpaceshipVisualEffects visualEffects, Transform transform)
        {
            _transform = transform;
            _uiManager = uiManager;
            _visualEffects = visualEffects;
            _speed = 0;
            _controlledByPlayer = transform.GetComponent<ControlledByPlayer>();
            CopyStats(baseData);
        }

        public string GetName()
        {
            return _name;
        }

        public float GetHealth()
        {
            return _health;
        }

        public float GetMaxHealth()
        {
            return _maxHealth;
        }

        public float GetMaxSpeed()
        {
            return _maxSpeed;
        }

        public float GetSpeed()
        {
            return _speed;
        }

        public void SetSpeed(float speed)
        {
            _speed = speed;
        }

        public float GetAcceleration()
        {
            return _acceleration;
        }

        public float GetDeceleration()
        {
            return _deceleration;
        }

        public float GetTurningSpeed()
        {
            return _turningSpeed;
        }

        public bool IsDead()
        {
            return _health <= 0;
        }

        public abstract void Damage(float damage, Vector3 sourceOfDamage);
        
        private void CopyStats(SpaceshipBaseData baseData)
        {
            _name = baseData._name;
            _health = baseData.maxHealth;
            _maxSpeed = baseData.maxSpeed;
            _acceleration = baseData.acceleration;
            _deceleration = baseData.decceleration;
            _turningSpeed = baseData.turningSpeed;
            _maxHealth = baseData.maxHealth;
            _minColorBoost = baseData.minimumColorBoost;
            _maxColorBoost = baseData.maximumColorBoost;
        }

        public Color GetMinColorBoost()
        {
            return _minColorBoost;
        }

        public Color GetMaxColorBoost()
        {
            return _maxColorBoost;
        }
    }
}