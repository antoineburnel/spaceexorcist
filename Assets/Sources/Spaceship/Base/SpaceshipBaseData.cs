namespace Ship
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "X.asset", menuName = "ScriptableObjects/Spaceship")]
    public class SpaceshipBaseData : ScriptableObject
    {
        public float maxHealth = 5;
        public string _name = "X";
        public float maxSpeed = 10;
        public float acceleration = 0.1f;
        public float decceleration = 0.05f;
        public float turningSpeed = 1f;
        public EnumGravityResitanceResistance gravityResistance = EnumGravityResitanceResistance.LOW;
        public Color minimumColorBoost;
        public Color maximumColorBoost;
        public float enemyVisionRange = 10f;
    }
}