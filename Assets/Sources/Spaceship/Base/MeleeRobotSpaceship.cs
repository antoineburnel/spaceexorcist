namespace Ship
{
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    public class MeleeRobotSpaceship : RobotSpaceship
    {
        public override void InitAI()
        {
            _ai = GetComponent<RobotSpaceshipAI>();
            _ai.Init(this);
            ZoneOfDetection detection = _transform.GetChild(2).GetChild(1).GetComponent<ZoneOfDetection>();
            detection.AddObserver(_ai);
        }

        public override void InitWeapon()
        {
        }
    }
}