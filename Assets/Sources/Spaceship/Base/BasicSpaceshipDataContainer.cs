using UnityEngine;
using Ship;
using Ship.Weapon;
using Ship.Camera;

public class BasicSpaceshipDataContainer : MonoBehaviour
{
    [SerializeField] private SpaceshipBaseData _baseShipData;
    [SerializeField] private WeaponBaseData _baseWeapon1Data;
    [SerializeField] private WeaponBaseData _baseWeapon2Data;
    [SerializeField] private CameraSettings _cameraSettings;

    public SpaceshipBaseData BaseShipData { get => _baseShipData; set => _baseShipData = value; }
    public WeaponBaseData BaseWeapon1Data { get => _baseWeapon1Data; set => _baseWeapon1Data = value; }
    public WeaponBaseData BaseWeapon2Data { get => _baseWeapon2Data; set => _baseWeapon2Data = value; }
    public CameraSettings CameraSettings { get => _cameraSettings; set => _cameraSettings = value; }
}
