namespace Ship
{

    using UnityEngine;

    public class PlayerSpaceshipData : SpaceshipData
    {
        public PlayerSpaceshipData(SpaceshipBaseData baseData, ISpaceshipUiManager uiManager, SpaceshipVisualEffects visualEffects, Transform transform) : base(baseData, uiManager, visualEffects, transform)
        {
        }

        public override void Damage(float damage, Vector3 sourceOfDamage)
        {
            if (IsDead())
            {   
                return;
            }
            _health -= damage;
            _visualEffects.DamageAnimation();
            if (_health > 0) {
                _uiManager.OnDamage(this, _controlledByPlayer);
                return;
            }
            _health = 0;
            _uiManager.OnDeath(_transform, _controlledByPlayer);
            _visualEffects.OnDeath(_transform, sourceOfDamage);
        }
    }
}