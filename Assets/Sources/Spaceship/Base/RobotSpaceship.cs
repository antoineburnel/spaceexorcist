namespace Ship
{
    using Utils;
    using UnityEngine;
    using Ship.Movement;
    using Ship.Weapon;
    using Bullets;
    using Ship.Animation;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(RobotSpaceshipAI))]
    public abstract class RobotSpaceship : Spaceship
    {
        protected RobotSpaceshipAI _ai;

        public void Init()
        {
            InitUi();
            InitBasicComponents();
            InitWeapon();
            InitAI();
            _shipState = new SpaceshipStateFlying();
            InitMovementController();
        }

        public override EnemySpaceshipAI GetAI()
        {
            return _ai;
        }

        public abstract void InitAI();

        protected void InitUi()
        {
            _uiManager = new EnemySpaceshipUiManager();
        }

        protected void InitMovementController()
        {
            _movementController = GetComponent<EnemySpaceshipController>();
            EnemySpaceshipController tmp = (EnemySpaceshipController)_movementController;
            tmp.Init(_rb, _shipData, _transform, _ai);
            _animator.DoUpdate(_shipData, _movementController.GetSpaceshipMovementData());
        }

        public override void InitializeAnimator()
        {
            _animator = _transform.GetComponent<EnemySpaceshipAnimator>();
            _animator.Init(_shipData);
        }

        public virtual void InitWeapon()
        {
            IWeaponControllerStrategy weaponStrategy = new EnemyWeaponControllerStrategy();
            BasicWeaponData weapon1Data = new BasicWeaponData(_datas.BaseWeapon1Data);
            BasicWeaponData weapon2Data = new BasicWeaponData(_datas.BaseWeapon2Data);
            _weaponController = new WeaponController(this, _transform, weapon1Data, weapon2Data, weaponStrategy);
            weaponStrategy.Init(_weaponController);
        }

        public override void InitializeSpaceshipData()
        {
            _shipData = new RobotSpaceshipData(_datas.BaseShipData, _uiManager, _visualEffects, _transform);
        }
        
        public void DoUpdate()
        {
            _animator.DoUpdate(_shipData, _movementController.GetSpaceshipMovementData());
        }

        public void DoFixedUpdate()
        {
            if (_shipState.CanMove())
            {
                _movementController.DoUpdate();
            }
        }

        public override void Hit(float damage, Transform enemyTransform, bool ignoreDamageProtection)
        {
            if (!_canTakeDamage && !ignoreDamageProtection)
            {
                return;
            }
            _shipData.Damage(damage, enemyTransform.position);
            if (_shipData.IsDead())
            {
                Destroy(gameObject);
                return;
            }
            bool damagedByBullet = enemyTransform.gameObject.layer == (int)LayerMeaning.PLAYER_BULLET || enemyTransform.gameObject.layer == (int)LayerMeaning.ALIEN_BULLET;
            bool damagedByMeleeWeapon = enemyTransform.gameObject.layer == (int)LayerMeaning.PLAYER_MELEE_WEAPON || enemyTransform.gameObject.layer == (int)LayerMeaning.ALIEN_MELEE_WEAPON;
            Transform shooterTransform;
            if (damagedByBullet)
            {
                shooterTransform = enemyTransform.GetComponent<Bullet>().GetEnemyTransform();
            }
            else if (damagedByMeleeWeapon)
            {
                shooterTransform = enemyTransform.GetComponent<ZoneOfDamage>().GetEnemyTransform();
            }
            else {
                return;
            }
            _ai.EVENT_OnReceivedDamage(shooterTransform);
        }

        public override void InPlanetGravity(float distance, float maxDistance)
        {
        }

        public override void InBlackHoleGravity(float distance, float maxDistance)
        {
        }

        public override void InitializeVisualEffects()
        {
            _visualEffects = new RobotSpaceshipVisualEffects(_transform);
        }

        public override void ChangingShip(Spaceship _newship, float duration)
        {
            _uiManager.ChangingShip(_newship, duration);
        }

        public override void ChangedShip(Spaceship _newship)
        {
            _uiManager.ChangedShip(_newship);
        }
    
        public override bool IsHuman()
        {
            return false;
        }

        public override bool IsAlien()
        {
            return false;
        }

        public override bool IsRobot()
        {
            return true;
        }

        public override bool IsEatable()
        {
            if (_isGettingEat) return false;
            return true;
        }

        public override bool IsParasitable()
        {
            return true;
        }

        public override bool IsHackable()
        {
            return true;
        }

        public override void OnOnDestroy()
        {
            Arena.Instance.OnEnemyDeath();
        }
    }
}