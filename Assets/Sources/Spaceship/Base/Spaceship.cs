namespace Ship
{
    using UnityEngine;
    using Ship.Animation;
    using Ship.Weapon;
    using Gravity;
    using Utils;
    using System.Collections.Generic;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SpaceshipAnimator))]
    [RequireComponent(typeof(BasicSpaceshipDataContainer))]
    [RequireComponent(typeof(SpaceshipMovementController))]
    public abstract class Spaceship : MonoBehaviour, ICanBeHit, ICanDie, IAllAttractionSensitive, IZoneSubject<Spaceship>
    {
        protected BasicSpaceshipDataContainer _datas;
        protected SpaceshipVisualEffects _visualEffects;
        private float _gravityMultiplier;
        protected SpaceshipData _shipData;
        protected SpaceshipMovementController _movementController;
        protected WeaponController _weaponController;
        protected Rigidbody2D _rb;
        protected Transform _transform;
        protected SpaceshipAnimator _animator;
        protected ISpaceshipUiManager _uiManager;
        protected SpaceshipState _shipState;
        protected IEventThatCanBeTriggered _actionE;
        protected IEventThatCanBeTriggered _actionA;
        protected Transform _initialParent;
        private List<IZoneObserver<Spaceship>> _observers = new List<IZoneObserver<Spaceship>>();
        protected bool _canTakeDamage = true;
        protected bool _isGettingEat = false;
        private List<IGravitySource> _gravityObservers = new List<IGravitySource>();

        public WeaponData GetWeaponData()
        {
            return _weaponController.GetData();
        }

        public WeaponData GetWeaponData(int index)
        {
            return _weaponController.GetData(index);
        }

        protected bool _isChangingShip;

        protected void InitBasicComponents()
        {
            _transform = transform;
            _initialParent = _transform.parent;
            _rb = _transform.GetComponent<Rigidbody2D>();
            _gravityMultiplier = _rb.mass;
            _datas = _transform.GetComponent<BasicSpaceshipDataContainer>();
            InitializeVisualEffects();
            InitializeSpaceshipData();
            InitializeAnimator();
            _isChangingShip = false;
        }

        public void SetState(SpaceshipState newState)
        {
            _shipState = newState;
        }

        public SpaceshipData GetData()
        {
            return _shipData;
        }

        public SpaceshipState GetState()
        {
            return _shipState;
        }

        public abstract void InitializeSpaceshipData();

        public abstract void InPlanetGravity(float distance, float maxDistance);

        public abstract void InBlackHoleGravity(float distance, float maxDistance);

        public virtual void InitializeVisualEffects()
        {
            _visualEffects = null;
        }

        public abstract void InitializeAnimator();

        public void DieFromBlackHole(Transform blackHole)
        {
            _visualEffects.SpriteCompression(blackHole);
        }

        public void Die(Transform enemyTransform)
        {
            Hit(float.MaxValue, enemyTransform, true);
        }

        public abstract void Hit(float damage, Transform enemyTransform, bool ignoreDamageProtection);

        public ISpaceshipUiManager GetUiManager()
        {
            return _uiManager;
        }

        public void ApplyPlanetGravity(Transform planetTransform, float gravityForce, float maxDistance)
        {
            float distance = Heuristics.euclideanDistance(planetTransform.position, _transform.position);
            if (distance > maxDistance) return;
            gravityForce = (1f - (distance - maxDistance)) * gravityForce * _gravityMultiplier;
            Vector3 direction = (planetTransform.position - _transform.position).normalized;
            _rb.AddForce(gravityForce * direction);
            InPlanetGravity(distance, maxDistance);
        }

        public void ApplyPlanetAntiGravity(Transform planetTransform, float antiGravityForce, float maxDistance)
        {
            float distance = Heuristics.euclideanDistance(planetTransform.position, _transform.position);
            if (distance > maxDistance) return;
            antiGravityForce = (1f - (distance - maxDistance)) * antiGravityForce * _gravityMultiplier;
            Vector3 direction = (_transform.position - planetTransform.position).normalized;
            _rb.AddForce(antiGravityForce * direction);
            InPlanetGravity(distance, maxDistance);
        }

        public void ApplyBlackHoleGravity(Transform planetTransform, float gravityForce, float maxDistance)
        {
            float distance = Heuristics.euclideanDistance(planetTransform.position, _transform.position);
            if (distance > maxDistance) return;
            gravityForce = (1f - (distance - maxDistance)) * gravityForce * _gravityMultiplier;
            Vector3 direction = (planetTransform.position - _transform.position).normalized;
            _rb.AddForce(gravityForce * direction);
            InBlackHoleGravity(distance, maxDistance);
        }

        public bool HasMoveE()
        {
            return _actionE != null;
        }

        public bool HasMoveA()
        {
            return _actionA != null;
        }

        public void SetMoveE(IEventThatCanBeTriggered action)
        {
            _actionE = action;
            _uiManager.SetMoveE(action.GetName(this));
        }

        public void UnsetMoveE()
        {
            _actionE = null;
            _uiManager.UnsetMoveE();
        }

        protected void TriggerE()
        {
            if (!_isChangingShip && _actionE != null)
            {
                _actionE.Trigger(this);
            }
        }

        public void SetMoveA(IEventThatCanBeTriggered action)
        {
            _actionA = action;
            _uiManager.SetMoveA(action.GetName(this));
        }

        public void UnsetMoveA()
        {
            _actionA = null;
            _uiManager.UnsetMoveA();
        }

        protected void TriggerA()
        {
            if (!_isChangingShip && _actionA != null)
            {
                _actionA.Trigger(this);
            }
        }

        public void TakeOff()
        {
            _shipState = _shipState.TakeOff();
        }

        public void Land()
        {
            _shipState = _shipState.Land();
            SetKinematic();
        }

        public void HasTookOff()
        {
            _shipState = _shipState.HasTookOff();
            UnsetKinematic();
            ResetParent();
        }

        public void HasLanded()
        {
            _shipState = _shipState.HasLanded();
        }

        public void SetSpeed(float speed)
        {
            _shipData.SetSpeed(speed);
        }

        public void ResetVelocity()
        {
            _movementController.ResetVelocity();
        }

        public void SetVelocity(Vector2 newVelocity)
        {
            _movementController.SetVelocity(newVelocity);
        }

        public float GetSpeed()
        {
            return _shipData.GetSpeed();
        }

        public float GetMaxSpeed()
        {
            return _shipData.GetMaxSpeed();
        }

        private void SetKinematic()
        {
            _rb.isKinematic = true;
        }

        private void UnsetKinematic()
        {
            _rb.isKinematic = false;
        }
    
        public abstract void ChangingShip(Spaceship _newship, float duration);

        public abstract void ChangedShip(Spaceship _newship);
    
        public void DisableColliders()
        {
            _transform.GetChild(1).gameObject.SetActive(false);
        }

        public void EnableColliders()
        {
            _transform.GetChild(1).gameObject.SetActive(true);
        }
    
        public void ChangeAllPlayerLayerToControlledPlayerLayer()
        {
            List<GameObject> objectsToModify = new List<GameObject>();
            objectsToModify.Add(gameObject);
            while (objectsToModify.Count > 0)
            {
                GameObject obj = objectsToModify[0];
                objectsToModify.RemoveAt(0);
                if (obj.layer == (int)LayerMeaning.PLAYER)
                {
                    obj.layer = (int)LayerMeaning.CONTROLLED_PLAYER;
                }
                for (int iChild = 0; iChild < obj.transform.childCount; iChild++)
                {
                    objectsToModify.Add(obj.transform.GetChild(iChild).gameObject);
                }
            }
        }

        public void ChangeAllControlledPlayerLayerToPlayerLayer()
        {
            List<GameObject> objectsToModify = new List<GameObject>();
            objectsToModify.Add(gameObject);
            while (objectsToModify.Count > 0)
            {
                GameObject obj = objectsToModify[0];
                objectsToModify.RemoveAt(0);
                if (obj.layer == (int)LayerMeaning.CONTROLLED_PLAYER)
                {
                    obj.layer = (int)LayerMeaning.PLAYER;
                }
                for (int iChild = 0; iChild < obj.transform.childCount; iChild++)
                {
                    objectsToModify.Add(obj.transform.GetChild(iChild).gameObject);
                }
            }
        }
    
        public Sprite GetWeaponSprite(int index)
        {
            return _transform.GetChild(0).GetChild(4).GetChild(1).GetChild(index).GetChild(0).GetComponent<SpriteRenderer>().sprite;
        }
    
        public void ResetParent()
        {
            _transform.SetParent(_initialParent);
        }
    
        public float GetTurnSpeed()
        {
            return _shipData.GetTurningSpeed();
        }
    
        public float GetVisionRange()
        {
            return _datas.BaseShipData.enemyVisionRange;
        }
    
        public abstract bool IsHuman();

        public abstract bool IsAlien();

        public abstract bool IsRobot();

        public abstract bool IsEatable();

        public abstract bool IsParasitable();

        public abstract bool IsHackable();

        public void SetUndamagable()
        {
            _canTakeDamage = false;
        }

        public void SetDamagable()
        {
            _canTakeDamage = true;
        }
    
        public void SetGettingEat()
        {
            _isGettingEat = true;
        }

        public void UnsetGettingEat()
        {
            _isGettingEat = false;
        }

        public abstract EnemySpaceshipAI GetAI();

        public void Register(IZoneObserver<Spaceship> observer)
        {
            _observers.Add(observer);
        }

        public void Unregister(IZoneObserver<Spaceship> observer)
        {
            if (_shipData.IsDead()) return;
            _observers.Remove(observer);
        }

        protected void UnregisterAll()
        {
            for (int i = _observers.Count-1; i >= 0; i--) {
                _observers.RemoveAt(i);
            }
            for (int i = _gravityObservers.Count-1; i >= 0; i--) {
                _gravityObservers.RemoveAt(i);
            }
        }

        public void NotifyDeath()
        {
            for (int i = 0; i < _observers.Count; i++)
            {
                _observers[i].OnDeath(this);
            }
        }

        private void OnDestroy()
        {
            OnOnDestroy();
            EnemySpaceshipAI.EVENT_DeclareDeath(this);
            NotifyDeath();
            GravitySignalDeath();
            UnregisterAll();
        }

        public abstract void OnOnDestroy();

        public void GravityRegister(IGravitySource source)
        {
            _gravityObservers.Add(source);
        }

        public void GravityUnregister(IGravitySource source)
        {
            _gravityObservers.Remove(source);
        }

        public void GravitySignalDeath()
        {
            foreach (IGravitySource source in _gravityObservers)
            {
                source.OnDeath(this);
            }
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}