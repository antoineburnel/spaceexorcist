using Ship;
using UnityEngine;
using System.Collections;

public class SwitchControlFromShipToShip : IEventThatCanBeTriggered
{
    private string _name;
    private Spaceship _baseship;
    private Spaceship _newship;
    private IEventThatCanBeTriggered _reversedAction;
    private bool _mustBeImmobile;
    private float _switchDuration;
    private bool _disableColliders;
    private bool _enableColliders;

    public SwitchControlFromShipToShip(string name)
    {
        _baseship = null;
        _newship = null;
        _reversedAction = null;
        _mustBeImmobile = false;
        _switchDuration = 1f;
        _disableColliders = false;
        _enableColliders = false;
        _name = name;
    }

    public SwitchControlFromShipToShip SwitchDuration(float duration)
    {
        _switchDuration = duration;
        return this;
    }

    public SwitchControlFromShipToShip From(Spaceship a)
    {
        _baseship = a;
        return this;
    }

    public SwitchControlFromShipToShip To(Spaceship b)
    {
        _newship = b;
        return this;
    }

    public SwitchControlFromShipToShip MustBeImmobile(bool value)
    {
        _mustBeImmobile = value;
        return this;
    }

    public SwitchControlFromShipToShip DisableColliders(bool value)
    {
        _disableColliders = value;
        return this;
    }

    public SwitchControlFromShipToShip EnableColliders(bool value)
    {
        _enableColliders = value;
        return this;
    }

    public SwitchControlFromShipToShip Bidirectional(string name)
    {
        _reversedAction = new SwitchControlFromShipToShip(name)
        .From(_newship)
        .To(_baseship)
        .MustBeImmobile(_mustBeImmobile)
        .SwitchDuration(_switchDuration)
        .EnableColliders(_disableColliders);
        return this;
    }

    public string GetName(Spaceship spaceship)
    {
        return _name;
    }

    public void Trigger(Spaceship spaceship)
    {

        if (spaceship.GetSpeed() > 0f && _mustBeImmobile)
        {
            return;
        }

        _baseship.StartCoroutine(GiveControlToNewShipAfter());
        _baseship.ChangingShip(_newship, _switchDuration);
        if (_disableColliders) _baseship.DisableColliders();
        _baseship.gameObject.GetComponent<ControlledByPlayer>().enabled = false;
    }

    private IEnumerator GiveControlToNewShipAfter()
    {
        yield return new WaitForSeconds(_switchDuration);

        if (_enableColliders)
        {
            _newship.EnableColliders();
            _newship.gameObject.SetActive(false);
            _newship.gameObject.SetActive(true);
        }

        _newship.GetComponent<ControlledByPlayer>().enabled = true;

        PlayerSpaceship playerSpaceship = _newship.GetComponent<PlayerSpaceship>();
        if (_reversedAction != null) playerSpaceship.SetMoveA(_reversedAction);

        _newship.ChangedShip(_newship);
        playerSpaceship.UpdateUI();
    }
}