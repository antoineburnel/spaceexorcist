using UnityEngine;

public class PlayerSpaceshipVisualEffects : SpaceshipVisualEffects
{

    private bool _onExplosion;

    public PlayerSpaceshipVisualEffects(Transform ship) : base(ship)
    {
        _onExplosion = false;
    }

    public override void OnDeath(Transform transform, Vector3 enemyPos)
    {
        if (_onExplosion)
        {
            return;
        }
        _onExplosion = true;
        ParticuleInitiatorSingleton.GetInstance().InstantiatePlayerExplosion(transform, enemyPos);
    }
}
