using UnityEngine;

public class RobotSpaceshipVisualEffects : SpaceshipVisualEffects
{

    private bool _onExplosion;

    public RobotSpaceshipVisualEffects(Transform ship) : base(ship)
    {
        _onExplosion = false;
    }

    public override void OnDeath(Transform transform, Vector3 enemyPos)
    {
        if (_onExplosion)
        {
            return;
        }
        _onExplosion = true;
        ParticuleInitiatorSingleton.GetInstance().InstantiateRobotExplosion(transform, enemyPos);
    }
}
