using UnityEngine;
using System.Collections;
using Ship;

public class MotherShipAutoTopRemove : MonoBehaviour
{

    private static float FADING_DURATION = 0.5f;

    [SerializeField] private GameObject _ceiling;
    [SerializeField] private GameObject _weapons;
    private SpriteRenderer _ceilingSprite;
    private SpriteRenderer _baseSprite;
    private SpriteRenderer _weapon1Sprite;
    private SpriteRenderer _weapon2Sprite;

    private Coroutine _animationCoroutine;

    private void Awake() {
        _ceilingSprite = _ceiling.GetComponent<SpriteRenderer>();
        _baseSprite = _weapons.transform.GetChild(0).GetComponent<SpriteRenderer>();
        _weapon1Sprite = _weapons.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        _weapon2Sprite = _weapons.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<SpriteRenderer>();
        _ceiling.SetActive(true);
        _weapons.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GetParentFromCollider parentColliderGetter = other.GetComponent<GetParentFromCollider>();
        if (parentColliderGetter == null)
            return;
        
        PlayerSpaceship player = parentColliderGetter.GetParentTransform().GetComponent<PlayerSpaceship>();
        if (player == null)  
            return;

        if (_animationCoroutine != null) {
            StopCoroutine(_animationCoroutine);
        }

        _animationCoroutine = StartCoroutine(MakeCeilingDisappear());
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        GetParentFromCollider parentColliderGetter = other.GetComponent<GetParentFromCollider>();
        if (parentColliderGetter == null)
            return;
        
        PlayerSpaceship player = parentColliderGetter.GetParentTransform().GetComponent<PlayerSpaceship>();
        if (player == null)  
            return;
        
        if (_animationCoroutine != null) {
            StopCoroutine(_animationCoroutine);
        }
        
        _animationCoroutine = StartCoroutine(MakeCeilingAppear());
    }

    private IEnumerator MakeCeilingAppear() {
        float elapsed = 0f;
        Color startColor = _ceilingSprite.color;
        Color endColor = startColor;
        endColor.a = 1f;
        while (elapsed < FADING_DURATION) {
            elapsed += Time.unscaledDeltaTime;
            _ceilingSprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _baseSprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _weapon1Sprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _weapon2Sprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            yield return null;
        }
        _ceilingSprite.color = endColor;
        _baseSprite.color = endColor;
        _weapon1Sprite.color = endColor;
        _weapon2Sprite.color = endColor;
    }

    private IEnumerator MakeCeilingDisappear() {
        float elapsed = 0f;
        Color startColor = _ceilingSprite.color;
        Color endColor = startColor;
        endColor.a = 0f;
        while (elapsed < FADING_DURATION) {
            elapsed += Time.unscaledDeltaTime;
            _ceilingSprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _baseSprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _weapon1Sprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            _weapon2Sprite.color = Color.Lerp(startColor, endColor, elapsed / FADING_DURATION );
            yield return null;
        }
        _ceilingSprite.color = endColor;
        _baseSprite.color = endColor;
        _weapon1Sprite.color = endColor;
        _weapon2Sprite.color = endColor;
    }
}