using UnityEngine;

public class AlienVisualEffects : SpaceshipVisualEffects
{

    private bool _onExplosion;

    public AlienVisualEffects(Transform ship) : base(ship)
    {
        _onExplosion = false;
    }

    public override void OnDeath(Transform transform, Vector3 enemyPos)
    {
        if (_onExplosion)
        {
            return;
        }
        _onExplosion = true;
        ParticuleInitiatorSingleton.GetInstance().InstantiateAlienExplosion(transform, enemyPos);
    }
}
