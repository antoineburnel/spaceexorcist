using UnityEngine;
using Utils;
using System.Collections;
using Ship;

public abstract class SpaceshipVisualEffects : ISpaceshipEvents
{
    private const float TIME_TO_GET_COMPRESSED = 0.5f;
    private VisualEffectsSingleton _visualEffects;
    private Transform _shipTransform;
    private Animator _animator;

    public SpaceshipVisualEffects(Transform ship)
    {
        _visualEffects = VisualEffectsSingleton.Instance;
        _shipTransform = ship;
        _animator = ship.GetChild(0).GetComponent<Animator>();
    }

    public void InBlackHole(float ratio)
    {
        _visualEffects.SetGlobalLightIntensity(ratio);
        _visualEffects.SetCameraBackgroundIntensity(ratio);
    }

    public void SetDangerSensation(float ratio)
    {
        float grainIntensity = Mathf.Clamp(ratio * 8, 0, 1);
        _visualEffects.SetFilmGrainIntensity(grainIntensity);

        float lensIntensity = Mathf.Clamp(ratio * 2, 0, 0.75f);
        _visualEffects.SetLensDistortion(-ratio);

        float vignetteIntensity = Mathf.Clamp(ratio * 2, 0, 0.5f);
        _visualEffects.SetVignette(vignetteIntensity);

        float saturation = Mathf.Clamp(-ratio * 100, -50, 0);
        _visualEffects.SetColorSaturation(saturation);

        float aberrationIntensity = Mathf.Clamp(ratio * 2, 0, 1f);
        _visualEffects.SetChromaticAberration(aberrationIntensity);
    }

    public void SetGravitySensation(float ratio)
    {
        float aberrationIntensity = ratio * 0.5f;
        _visualEffects.SetChromaticAberration(aberrationIntensity);

        float LensDistortion = ratio * 0.33f;
        _visualEffects.SetLensDistortion(LensDistortion);

        float filmGrain = ratio * 0.8f;
        _visualEffects.SetFilmGrainIntensity(filmGrain);
    }

    public void DamageAnimation()
    {
        _animator.SetTrigger("damage");
    }

    public void SpriteCompression(Transform blackHole)
    {
        _shipTransform.GetComponent<MonoBehaviour>().StartCoroutine(AnimateCompression(blackHole));
    }

    private IEnumerator AnimateCompression(Transform blackHole)
    {
        float elapsed = 0f;
        float t = 0f;

        Vector3 initialScale = _shipTransform.localScale;
        Vector3 finalScale = new Vector3(0f, 0f, 1f);

        while (elapsed < TIME_TO_GET_COMPRESSED)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_GET_COMPRESSED;

            _shipTransform.localScale = Vector3.Lerp(initialScale, finalScale, SmoothFunction.EaseInSquare(t));

            yield return null;
        }

        _shipTransform.localScale = finalScale;
        _shipTransform.GetComponent<Spaceship>().Die(blackHole);
    }

    public abstract void OnDeath(Transform transform, Vector3 enemyTransfrom);
}
