using UnityEngine;
using System.Collections;

public class AlienEgg : MonoBehaviour
{
    [SerializeField] private float _timeBeforeExplosion = 2f;
    [SerializeField] private float _timeToExplode = 1f;
    [SerializeField] private float _timeToSpawn = 0.75f;
    [SerializeField] private GameObject _alienPrefab;

    private Coroutine _coroutine;

    private void Awake()
    {
        _coroutine = StartCoroutine(anim());
    }

    private IEnumerator anim()
    {
        yield return new WaitForSeconds(_timeBeforeExplosion);
        transform.GetComponent<Animator>().SetTrigger("explode");
        yield return new WaitForSeconds(_timeToSpawn);
        Arena.Instance.OnNewEnemy();
        Instantiate(_alienPrefab, transform.position, Quaternion.identity, ParentSingleton.GetInstance().GetShipParent());
        yield return new WaitForSeconds(_timeToExplode - _timeToSpawn);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }
    }
}
