using UnityEngine;

public class ParentSingleton : MonoBehaviour
{
    [SerializeField] private Transform _ship;

    private static ParentSingleton instance;

    private void Awake()
    {
        instance = this;
    }

    public static ParentSingleton GetInstance()
    {
        return instance;
    }

    public Transform GetShipParent()
    {
        return _ship;
    }
}
