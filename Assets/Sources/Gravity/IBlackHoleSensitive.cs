namespace Gravity
{
    using UnityEngine;

    public interface IBlackHoleSensitive : ICanFeelGravity
    {
        public void ApplyBlackHoleGravity(Transform planetTransform, float gravityForce, float maxDistance);
        public void DieFromBlackHole(Transform blackHoleTransform);
    }
}