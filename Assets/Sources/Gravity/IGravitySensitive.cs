namespace Gravity
{
    using UnityEngine;

    public interface IGravitySensitive : ICanFeelGravity
    {
        public void ApplyPlanetGravity(Transform planetTransform, float gravityForce, float maxDistance);
    }
}