namespace Gravity
{
    using UnityEngine;

    public interface IAntiGravitySensitive : ICanFeelGravity
    {
        public void ApplyPlanetAntiGravity(Transform planetTransform, float antiGravityForce, float maxDistance);
    }
}