namespace Gravity
{
    public interface IGravitySource
    {
        public void OnDeath(ICanFeelGravity sensitive);
    }
}