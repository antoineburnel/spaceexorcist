namespace Gravity
{
    using UnityEngine;
    using System.Collections.Generic;

    public class BlackHoleGravity : MonoBehaviour, IGravitySource
    {
        private float _attractionRadius;
        private List<ICanFeelGravity> _gravityObjects;
        private Transform _transform;
        [SerializeField] private float _gravityForce;
        private float _maxEuclideanDistance;

        private void Awake ()
        {
            _transform = transform;
            _gravityObjects = new List<ICanFeelGravity>();
            _attractionRadius = _transform.GetComponent<CircleCollider2D>().radius;
            _maxEuclideanDistance = _attractionRadius * _transform.lossyScale.x;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
            if ( parent == null )
            {
                return;
            }

            ICanFeelGravity gravityObject = parent.GetParentTransform().GetComponent<IBlackHoleSensitive>();
            if ( gravityObject == null )
            {
                return;
            }

            _gravityObjects.Add( gravityObject );
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
            if ( parent == null )
            {
                return;
            }

            ICanFeelGravity gravityObject = parent.GetParentTransform().GetComponent<IBlackHoleSensitive>();
            if ( gravityObject == null )
            {
                return;
            }

            gravityObject.GravityUnregister(this);
            _gravityObjects.Remove( gravityObject );
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < _gravityObjects.Count; i++) 
            {
                IBlackHoleSensitive gravityObject = (IBlackHoleSensitive) _gravityObjects[i];
                gravityObject.ApplyBlackHoleGravity(_transform, _gravityForce, _maxEuclideanDistance);
            }
        }

        public void OnDeath(ICanFeelGravity sensitive)
        {
            _gravityObjects.Remove(sensitive);
        }
    }
}