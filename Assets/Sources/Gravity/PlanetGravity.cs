namespace Gravity
{
    using UnityEngine;
    using System.Collections.Generic;
    using System;

    public class PlanetGravity : MonoBehaviour, IGravitySource
    {
        private float _attractionRadius;
        private List<ICanFeelGravity> _gravityObjects;
        private Transform _transform;
        [SerializeField] private float _gravityForce;
        private float _maxEuclideanDistance;

        private void Awake ()
        {
            _transform = transform;
            _gravityObjects = new List<ICanFeelGravity>();
            _attractionRadius = GetComponent<CircleCollider2D>().radius;
            _maxEuclideanDistance = _attractionRadius * _transform.lossyScale.x;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
            if ( parent == null )
            {
                return;
            }

            IGravitySensitive gravityObject = parent.GetParentTransform().GetComponent<IGravitySensitive>();
            if ( gravityObject == null )
            {
                return;
            }

            gravityObject.GravityRegister(this);
            _gravityObjects.Add( gravityObject );
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            GetParentFromCollider parent = other.GetComponent<GetParentFromCollider>();
            if ( parent == null )
            {
                return;
            }

            IGravitySensitive gravityObject = parent.GetParentTransform().GetComponent<IGravitySensitive>();
            if ( gravityObject == null )
            {
                return;
            }

            gravityObject.GravityUnregister(this);
            _gravityObjects.Remove( gravityObject );
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < _gravityObjects.Count; i++) 
            {
                IGravitySensitive gravityObject = (IGravitySensitive) _gravityObjects[i];
                gravityObject.ApplyPlanetGravity(_transform, _gravityForce, _maxEuclideanDistance);
            }
        }

        public void OnDeath(ICanFeelGravity sensitive)
        {
            _gravityObjects.Remove(sensitive);
        }
    }
}