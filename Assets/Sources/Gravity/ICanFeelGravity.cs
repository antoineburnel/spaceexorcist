namespace Gravity
{
	public interface ICanFeelGravity
	{
		public void GravityRegister(IGravitySource source);
		public void GravityUnregister(IGravitySource source);
		public void GravitySignalDeath();
	}
}