namespace Gravity
{
    public interface IAllAttractionSensitive : IAntiGravitySensitive, IGravitySensitive, IBlackHoleSensitive {

    }

}