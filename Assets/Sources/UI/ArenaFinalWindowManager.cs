using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using Utils;
using TMPro;

public class ArenaFinalWindowManager : MonoBehaviour
{
    private const float TIME_TO_SHOW = 1f;

    private Transform _transform;
    private Vector2 _hiddenPosition = new Vector2(0, 1000);
    private Vector2 _visiblePosition = new Vector2(0, 0);
    private Color32 _winColor = new Color32(180, 245, 175, 255);
    private Color32 _loseColor = new Color32(246, 175, 175, 255);

    [SerializeField] private TMP_Text _mainText;
    [SerializeField] private TMP_Text _waveNumber;
    [SerializeField] private TMP_Text _timer;

    private void Awake()
    {
        _transform = transform;
        _transform.localPosition = _hiddenPosition;
        gameObject.SetActive(false);
    }

    public void OnVictory(int waveNumber, int nbOfWaves, int minutes, int seconds, int milliseconds)
    {
        _mainText.SetText("Victory");
        _mainText.color = _winColor;
        _waveNumber.SetText("Wave " + waveNumber + "/" + nbOfWaves);
        _timer.SetText(string.Format("{0}:{1:00}:{2:000}", minutes, seconds, milliseconds));
        gameObject.SetActive(true);
        StartCoroutine(ShowWindow());
    }

    public void OnDefeat(int waveNumber, int nbOfWaves)
    {
        _mainText.SetText("Defeat");
        _mainText.color = _loseColor;
        _waveNumber.SetText("Wave " + waveNumber + "/" + nbOfWaves);
        _timer.SetText("-:--:---");
        gameObject.SetActive(true);
        StartCoroutine(ShowWindow());
    }

    public void ACTION_Quit()
    {
        Application.Quit(0);
    }

    public void ACTION_Restart()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

    private IEnumerator ShowWindow()
    {
        VisualEffectsSingleton visualEffectsSingleton = VisualEffectsSingleton.Instance;

        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_SHOW;

            Time.timeScale = Mathf.Lerp(1f, 0f, SmoothFunction.EaseOutCubic(t));
            _transform.localPosition = Vector3.Lerp(_hiddenPosition, _visiblePosition, SmoothFunction.EaseOutCubic(t));
            visualEffectsSingleton.SetDepthOfField(Mathf.Lerp(1f, 0f, SmoothFunction.EaseOutCubic(t)));
            
            yield return null;
        }

        Time.timeScale = 0f;
        _transform.localPosition = _visiblePosition;
        visualEffectsSingleton.SetDepthOfField(0f);
    }
}
