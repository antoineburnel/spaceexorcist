using TMPro;
using UnityEngine;
using System.Collections;

public class FPScounter : MonoBehaviour
{
    private TMP_Text _text;
    [SerializeField] private float _updateInterval = 0.5f;
    private float _accum;
    private int _frames;

    private void Awake()
    {
        _accum = 0f;
        _frames = 0;
        _text = GetComponent<TMP_Text>();
        StartCoroutine( UpdateCounterAfter(_updateInterval) );
    }

    private void Update()
    {
        _accum += Time.timeScale / Time.unscaledDeltaTime;
        _frames++;
    }

    private IEnumerator UpdateCounterAfter(float delay)
    {
        yield return new WaitForSeconds(delay);
        _text.SetText( (int)(_accum / _frames) + " FPS" );
        _accum = 0f;
        _frames = 0;
        StartCoroutine( UpdateCounterAfter(delay) );
    }
}
