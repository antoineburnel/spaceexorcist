public interface IActivableUI
{
    public void Init();
    public void ActivateUI ();
    public void DesactivateUI ();
    public void Show ();
    public void Hide ();
}
