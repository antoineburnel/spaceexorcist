using UnityEngine;
using System.Collections;

public class AlwaysRotateTowardsCamera : MonoBehaviour
{
    [SerializeField] private float _UpdatePeriodSeconds = 0.25f;
    private Transform _cameraTransform;
    private Transform _transform;
    private Coroutine _coroutine;

    private void Start()
    {
        _cameraTransform = Camera.main?.transform;
        _transform = transform;

        if (_cameraTransform == null)
        {
            Debug.LogError("Camera.main not found!");
            return;
        }
        
        _coroutine = StartCoroutine(UpdateRotation(_UpdatePeriodSeconds));
    }

    private IEnumerator UpdateRotation(float waitTime)
    {
        yield return new WaitForSecondsRealtime(_UpdatePeriodSeconds);
        _transform.rotation = _cameraTransform.rotation;
        _coroutine = StartCoroutine(UpdateRotation(waitTime));
    }
}

