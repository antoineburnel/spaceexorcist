using UnityEngine;
using System.Collections;
using Utils;
using TMPro;

public class WaveNumberUI : MonoBehaviour
{
    private Transform _transform;
    private Transform _textsTransform;
    private Transform _text1Transform;
    private TMP_Text _text1;
    private TMP_Text _text2;

    private const float TIME_BEFORE_ALL_STARTS = 2f;
    private const float TIME_TO_SHOW = 1f;
    private const float TIME_TO_HIDE = 1f;
    private const float TIME_TO_INCREASE_TEXT_SIZE = 1f;
    private const float DURATION_SHOWING_WAVE = 1f;
    private const float DURATION_SHOWING_CLEARED = 1f;
    private const int SMALL_TEXT_FONT_SIZE = 45;
    private const int BIG_TEXT_FONT_SIZE = 70;
    
    private Vector3 _TEXT1_SMALL_POS = new Vector3(0, 50, 0);
    private Vector3 _TEXT1_BIG_POS = new Vector3(0, 0, 0);
    private Vector3 _visiblePosition = new Vector3(0, 0, 0);
    private Vector3 _hiddenPosition = new Vector3(0, 1000, 0);

    private int _numberOfWaves;

    private void Awake()
    {
        _transform = transform;
        _textsTransform = _transform.GetChild(0);
        _text1Transform = _textsTransform.GetChild(1);
        _text1 = _text1Transform.GetComponent<TMP_Text>();
        _text2 = _textsTransform.GetChild(2).GetComponent<TMP_Text>();
        _textsTransform.localPosition = _hiddenPosition;
    }
    
    public float OnArenaBegin(int cooldown, int numberOfWaves)
    {
        _numberOfWaves = numberOfWaves;
        _text1.SetText("Starting in");
        _text2.SetText(cooldown.ToString());
        StartCoroutine(ShowText(TIME_BEFORE_ALL_STARTS));
        int n = 1;
        for (int i = cooldown-1; i >= 0; i--)
        {
            StartCoroutine(SetText2(TIME_TO_SHOW + n + TIME_BEFORE_ALL_STARTS, i.ToString()));
            n++;
        }
        StartCoroutine(SetText1(TIME_TO_SHOW + n + TIME_BEFORE_ALL_STARTS, "Wave 1/" + _numberOfWaves));
        StartCoroutine(SetText2(TIME_TO_SHOW + n + TIME_BEFORE_ALL_STARTS, ""));
        StartCoroutine(IncreaseSizeText1(TIME_TO_SHOW + n + TIME_BEFORE_ALL_STARTS));
        StartCoroutine(HideText(TIME_TO_SHOW + n + DURATION_SHOWING_WAVE + TIME_TO_INCREASE_TEXT_SIZE + TIME_BEFORE_ALL_STARTS));
        return TIME_TO_SHOW + n + TIME_TO_INCREASE_TEXT_SIZE + DURATION_SHOWING_WAVE + TIME_TO_HIDE + TIME_BEFORE_ALL_STARTS;
    }

    public float OnWaveOver(int newWaveNumber)
    {
        _text1.SetText("Cleared");
        StartCoroutine(ShowText(0));
        StartCoroutine(SetText1(TIME_TO_SHOW + DURATION_SHOWING_CLEARED, "Wave " + newWaveNumber + "/" + _numberOfWaves));
        StartCoroutine(HideText(TIME_TO_SHOW + DURATION_SHOWING_CLEARED + DURATION_SHOWING_WAVE));
        return TIME_TO_SHOW + DURATION_SHOWING_CLEARED + DURATION_SHOWING_WAVE + TIME_TO_HIDE;
    }

    private IEnumerator ShowText(float waitDuration)
    {
        yield return new WaitForSecondsRealtime(waitDuration);
        _textsTransform.gameObject.SetActive(true);
        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_SHOW;
            _textsTransform.localPosition = Vector3.Lerp(_hiddenPosition, _visiblePosition, SmoothFunction.EaseOutSquare(t));
            yield return null;
        }

        _textsTransform.localPosition = _visiblePosition;
    }

    private IEnumerator HideText(float waitDuration)
    {
        yield return new WaitForSecondsRealtime(waitDuration);
        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_HIDE)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_HIDE;
            _textsTransform.localPosition = Vector3.Lerp(_visiblePosition, _hiddenPosition, SmoothFunction.EaseInSquare(t));
            yield return null;
        }

        _textsTransform.localPosition = _hiddenPosition;
        _textsTransform.gameObject.SetActive(false);
    }

    private IEnumerator SetText1(float waitDuration, string text)
    {
        yield return new WaitForSecondsRealtime(waitDuration);
        _text1.SetText(text);
    }

    private IEnumerator SetText2(float waitDuration, string text)
    {
        yield return new WaitForSecondsRealtime(waitDuration);
        _text2.SetText(text);
    }

    private IEnumerator IncreaseSizeText1(float waitDuration)
    {
        yield return new WaitForSecondsRealtime(waitDuration);
        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_INCREASE_TEXT_SIZE;
            _text1.fontSize = Mathf.Lerp(SMALL_TEXT_FONT_SIZE, BIG_TEXT_FONT_SIZE, SmoothFunction.EaseOutSquare(t));
            _text1Transform.localPosition = Vector3.Lerp(_TEXT1_SMALL_POS, _TEXT1_BIG_POS, SmoothFunction.EaseOutSquare(t));
            yield return null;
        }

        _text1.fontSize = BIG_TEXT_FONT_SIZE;
        _text1Transform.localPosition = _TEXT1_BIG_POS;
    }
}