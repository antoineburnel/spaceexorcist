using UnityEngine;

public class CanvasController : MonoBehaviour
{
    private void Awake()
    {
        Time.timeScale = 1f;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseMenuManager.Instance.ACTION_SwitchPause();
        }
    }
}