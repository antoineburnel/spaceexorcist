using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class PauseMenuManager : MonoBehaviour
{
    public static PauseMenuManager Instance;

    private const float TIME_TO_SHOW = 0.5f;
    private const float TIME_TO_HIDE = 0.5f;

    private Transform _transform;
    private Transform _buttonsTransform;
    private Vector3 _visibleButtonsPos = new Vector3(0, 0, 0);
    private Vector3 _HiddenButtonsPos = new Vector3(0, 1000, 0);
    private Transform _bgTransform;

    private bool _isPaused;

    private Coroutine _coroutine;

    private void Awake()
    {
        Instance = this;
        _isPaused = false;
        _transform = transform;
        _bgTransform = _transform.GetChild(0);
        _buttonsTransform = _bgTransform.GetChild(0);
        _bgTransform.gameObject.SetActive(false);
    }

    public void ACTION_Pause()
    {
        if (_coroutine != null || _isPaused) return;
        _isPaused = true;
        _bgTransform.gameObject.SetActive(true);
        _coroutine = StartCoroutine(PauseAnim());
    }

    public void ACTION_Resume()
    {
        if (_coroutine != null || !_isPaused) return;
        _isPaused = false;
        _coroutine = StartCoroutine(ResumeAnim());
    }

    public void ACTION_SwitchPause()
    {
        if (_coroutine != null) return;
        if (_isPaused) ACTION_Resume();
        else ACTION_Pause();
    }

    private IEnumerator PauseAnim()
    {
        VisualEffectsSingleton visualEffectsSingleton = VisualEffectsSingleton.Instance;

        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_SHOW)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_SHOW;

            _buttonsTransform.localPosition = Vector3.Lerp(_HiddenButtonsPos, _visibleButtonsPos, SmoothFunction.EaseInOutSquare(t));
            Time.timeScale = Mathf.Lerp(1f, 0f, SmoothFunction.EaseInOutSquare(t));
            visualEffectsSingleton.SetDepthOfField(Mathf.Lerp(1f, 0f, SmoothFunction.EaseInOutSquare(t)));

            yield return null;
        }

        _buttonsTransform.localPosition = _visibleButtonsPos;
        visualEffectsSingleton.SetDepthOfField(0f);
        _coroutine = null;
        Time.timeScale = 0f;
    }

    private IEnumerator ResumeAnim()
    {
        VisualEffectsSingleton visualEffectsSingleton = VisualEffectsSingleton.Instance;

        float elapsed = 0f;
        float t = 0f;

        while (elapsed < TIME_TO_HIDE)
        {
            elapsed += Time.unscaledDeltaTime;
            t = elapsed / TIME_TO_HIDE;

            _buttonsTransform.localPosition = Vector3.Lerp(_visibleButtonsPos, _HiddenButtonsPos, SmoothFunction.EaseInOutSquare(t));
            Time.timeScale = Mathf.Lerp(0f, 1f, SmoothFunction.EaseInOutSquare(t));
            visualEffectsSingleton.SetDepthOfField(Mathf.Lerp(0f, 1f, SmoothFunction.EaseInOutSquare(t)));

            yield return null;
        }

        _buttonsTransform.localPosition = _HiddenButtonsPos;
        _bgTransform.gameObject.SetActive(false);
        visualEffectsSingleton.SetDepthOfField(1f);
        _coroutine = null;
        Time.timeScale = 1f;
    }

    public void ACTION_Options()
    {
        Debug.Log("Options");
    }

    public void ACTION_Quit()
    {
        Application.Quit(0);
    }
}