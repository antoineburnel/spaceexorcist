using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;
using Ship;

public class Arena : MonoBehaviour
{
    private static Arena _instance;

    public static Arena Instance => _instance;

    [SerializeField] private Vector2 _arenaDimensions;
    [SerializeField] private Transform _shipSpawnerParent;
    [SerializeField] private float _timeBetweenEnemySpawn = 0.1f;
    [SerializeField] private LayerMask _areaNotToSpawn;
    [SerializeField] private List<Wave> _waves;
    [SerializeField] private GameObject _prefabSpawnExplosion;
    [SerializeField] private ArenaFinalWindowManager _finalWindowManager;
    [SerializeField] private WaveNumberUI _waveNumberUI;
    private const float _COLLIDER_SIZE = 3f;
    private const float _MINIMAL_DISTANCE_FROM_PLAYER = 10f;
    private const int _ARENA_START_COOLDOWN = 5;
    private const float _DURATION_BETWEEN_WAVE_ANNOUNCEMENT_AND_SPAWN = 0.5f;
    private TimerTextUISystem _timerUI;
    private Coroutine _spawnCoroutine;
    private int _waveIndex;
    [SerializeField] private Transform _player;
    private PlayerSpaceship _playerSpaceship;
    private int _currentNbEnemiesInArena;
    private bool _arenaOver;

    private void Awake()
    {
        _instance = this;
        _arenaOver = false;
        _waveIndex = -1;
        _playerSpaceship = _player.GetComponent<PlayerSpaceship>();

        StartCoroutine(StartArenaAfter(_ARENA_START_COOLDOWN));
    }

    private IEnumerator StartArenaAfter(int cooldown)
    {
        float timeToWait = _waveNumberUI.OnArenaBegin(cooldown, _waves.Count);
        yield return new WaitForSecondsRealtime(timeToWait + _DURATION_BETWEEN_WAVE_ANNOUNCEMENT_AND_SPAWN);
        NewWave();
    }

    public void Inject(TimerTextUISystem timerUI)
    {
        _timerUI = timerUI;
    }

    public void OnEnemyDeath()
    {
        _currentNbEnemiesInArena--;
        if (_currentNbEnemiesInArena == 0)
        {
            NewWave();
        }
    }

    public void OnPlayerDeath()
    {
        PlayerLost();
    }

    private void NewWave()
    {
        _waveIndex++;
        if (_waveIndex >= _waves.Count)
        {
            PlayerWon();
            return;
        }
        StartCoroutine(SpawnWave(_waveIndex));
    }

    public void OnNewEnemy()
    {
        _currentNbEnemiesInArena++;
    }

    private void PlayerWon()
    {
        if (_arenaOver == true) return;
        _playerSpaceship.GetUiManager().Hide();
        _timerUI.StopClock();
        _arenaOver = true;
        int minutes = _timerUI.GetMinutes();
        int seconds = _timerUI.GetSeconds();
        int milliseconds = _timerUI.GetMilliseconds();
       _finalWindowManager.OnVictory(_waveIndex, _waves.Count, minutes, seconds, milliseconds);
    }

    private void PlayerLost()
    {
        if (_arenaOver == true) return;
        _playerSpaceship.GetUiManager().Hide();
        _timerUI.StopClock();
        _arenaOver = true;
        _finalWindowManager.OnDefeat(_waveIndex, _waves.Count);
    }

    private void FixedUpdate()
    {
        _timerUI?.UpdateText();
    }

    private IEnumerator SpawnWave(int index)
    {
        if (index != 0) {
            float timeToWait = _waveNumberUI.OnWaveOver(index+1);
            yield return new WaitForSecondsRealtime(timeToWait);
        }
        yield return new WaitForSecondsRealtime(_DURATION_BETWEEN_WAVE_ANNOUNCEMENT_AND_SPAWN);

        LinkedList<int> prefabIDs = new LinkedList<int>();
        int nbUniqPrefabs = _waves[index].enemies.Count;
        int n = 0;
        for (int i = 0; i < nbUniqPrefabs; i++)
        {
            for (int j = 0; j < _waves[index].nb[i]; j++)
            {
                prefabIDs.AddLast(i);
                n++;
            }
        }

        prefabIDs = Shuffle(prefabIDs, n);

        n = 0;
        foreach (int i in prefabIDs)
        {
            StartCoroutine(SpawnEnemy(n * _timeBetweenEnemySpawn, _waveIndex, i));
            n++;
        }

        _currentNbEnemiesInArena = n;

        if (index == 0) _timerUI.StartClock();
    }

    private LinkedList<int> Shuffle(LinkedList<int> list, int size)
    {
        return new LinkedList<int>(list.OrderBy((o) =>
        {
            return (Random.Range(0, size));
        }));
    }

    private IEnumerator SpawnEnemy(float waitTimeSeconds, int waveIndex, int prefabIndex)
    {
        yield return new WaitForSecondsRealtime(waitTimeSeconds);
        float spawnAngleZ = Random.Range(0f, 360f);
        Quaternion spawnRot = Quaternion.Euler(0f, 0f, spawnAngleZ);
        Vector3 spawnPos = GetRandomCoords();
        GameObject enemyPrefab = _waves[waveIndex].enemies[prefabIndex];
        Instantiate(enemyPrefab, spawnPos, spawnRot, _shipSpawnerParent);
        Instantiate(_prefabSpawnExplosion, spawnPos, Quaternion.identity);
    }

    private Vector3 GetRandomCoords()
    {
        Vector3 spawnPos;
        int i = 0;
        while (true) {
            if (i == 50) { Debug.LogError("Tried to find a spawn pos 50 times ..."); return Vector3.positiveInfinity; }
            spawnPos = new Vector3(Random.Range(0, _arenaDimensions.x), Random.Range(0, _arenaDimensions.y), 0f);
            Collider2D colliders = Physics2D.OverlapCircle(spawnPos, _COLLIDER_SIZE, _areaNotToSpawn);
            float distanceFromPlayer = Heuristics.euclideanDistance(spawnPos, _player.position);
            if (colliders == null && distanceFromPlayer >= _MINIMAL_DISTANCE_FROM_PLAYER)
            {
                break;
            }
            i++;
        }
        return spawnPos;
    }
}