using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private float _nbOfStarsPerM2 = 1;
    [SerializeField] private Vector2 _dimensions = new Vector2(100, 100);
    [SerializeField] private Vector2 _origin = new Vector2(0, 0);
    [SerializeField] private Transform _camera;
    [SerializeField] private BackgroundLayers _starBackground;

    private void Awake()
    {
        _starBackground.GetComponent<BackgroundLayers>();
    }

    private void Start()
    {
        _starBackground.Inject(_nbOfStarsPerM2, _origin, _dimensions, _camera);
    }

    private void FixedUpdate()
    {
        _starBackground.UpdateLayers();
    }
}