using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObjects/Wave", order = 0)]
public class Wave : ScriptableObject
{
    public List<GameObject> enemies;
    public List<int> nb;
}