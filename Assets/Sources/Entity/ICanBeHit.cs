using UnityEngine;
public interface ICanBeHit
{
    public void Hit(float damage, Transform ennemyTransform, bool ignoreDamageProtection);
}
