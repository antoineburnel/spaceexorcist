using Ship;
public interface IEventThatCanBeTriggered
{
    public void Trigger ( Spaceship spaceship );
    public string GetName ( Spaceship spaceship );
}
