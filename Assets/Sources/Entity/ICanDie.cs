using UnityEngine;
public interface ICanDie
{
    public void Die ( Transform ennemyTransform );
}
