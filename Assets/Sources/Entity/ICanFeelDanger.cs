using UnityEngine;
public interface ICanFeelDanger
{
    public void InDanger ( Transform source, float maxDistance );
    public void ResetDanger ();
}
