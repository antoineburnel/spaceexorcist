using UnityEngine;

public class ParticuleInitiatorSingleton : MonoBehaviour
{
    private static ParticuleInitiatorSingleton _instance;
    private ParticuleInitiatorSingleton () { }

    private void Awake ()
    {
        _instance = this;   
    }

    public static ParticuleInitiatorSingleton GetInstance ()
    {
        return _instance;
    }

    [SerializeField] private GameObject prefabPlayerExplosion;
    [SerializeField] private GameObject prefabRobotExplosion;
    [SerializeField] private GameObject prefabAlienExplosion;
    private static float DURATION_PLAYER_EXPLOSION = 1.3f;
    private static float DURATION_ROBOT_EXPLOSION = 1.3f;
    private static float DURATION_ALIEN_EXPLOSION = 1.3f;

    public GameObject InstantiatePlayerExplosion ( Transform myTransform, Vector3 enemyPosition )
    {
        return InstantiateExplosion(myTransform, enemyPosition, prefabPlayerExplosion, DURATION_PLAYER_EXPLOSION);
    }

    public GameObject InstantiateRobotExplosion ( Transform myTransform, Vector3 enemyPosition )
    {
        return InstantiateExplosion(myTransform, enemyPosition, prefabRobotExplosion, DURATION_ROBOT_EXPLOSION);
    }

    public GameObject InstantiateAlienExplosion ( Transform myTransform, Vector3 enemyPosition )
    {
        return InstantiateExplosion(myTransform, enemyPosition, prefabAlienExplosion, DURATION_ALIEN_EXPLOSION);
    }

    private GameObject InstantiateExplosion(Transform myTransform, Vector3 enemyPosition, GameObject prefabExplosion, float duration)
    {
        //Quaternion rot = Quaternion.LookRotation(myTransform.position - enemyPosition, Vector3.back);
        return Instantiate(prefabExplosion, myTransform.position, Quaternion.identity);
    }
}
