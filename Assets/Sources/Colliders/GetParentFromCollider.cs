using UnityEngine;

public class GetParentFromCollider : MonoBehaviour
{ 
    [SerializeField] private Transform _parent;

    public Transform GetParentTransform()
    {
        return _parent;
    }
}
