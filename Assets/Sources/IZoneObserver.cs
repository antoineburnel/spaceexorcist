public interface IZoneObserver<T>
{
    public void OnDeath(T subject);
}